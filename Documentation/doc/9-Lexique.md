
Opensource 
Un savoir développé et géré dans le cadre d'une collaboration ouverte, et mis à disposition, généralement gratuitement, pour que chacun puisse l'utiliser, l'examiner, le modifier et le redistribuer comme il le souhaite.

compostabilité




# Lexique

### A/
***AoP - Aire de protection***
L'aire de protection (AoP) est ce que le projet s'efforcent de préserver et d'améliorer. Il s'agit généralement du bien-être social ou humain. La zone de protection est traduite par un ou plusieurs Endpoints qui fournissent des informations pertinentes sur l'évolution de ses conditions. 

### B/
***Backfire***
Un Backfire se produit lorsque les avantages d'une augmentation de l'efficacité énergétique sont moins importants que l'effet de rebond qui s'ensuit.

### C/
***Capabilité***
La capabilité d'une personne est définie comme l'étendue des possibilités réelles que possède un individu de faire et d'être.
-En résumé, à partir de dotation (endowment/potentialité), un individu va chercher à convertir ses dotations en accomplissement à l'aide de fonction d'utilisation. L'image de ses dotations par la fonction d'utilisation est la capabilité de l'individu. L'accomplissement est la part des capabilités qui s'accomplissent.

### E/

***Empreinte fantôme***
L'empreinte fantôme se définit comme étant: l'ensemble des liens qu'une machine tisse avec le reste du monde.

***ELCA - Environmental Life Cycle Analysis***
L'ELCA (ou E-LCA), communément appelée ACV, est une méthode d'évaluation des impacts environnementaux associés à toutes les étapes du cycle de vie d'un produit, d'un processus ou d'un service commercial.

***Endpoint Impact***
L'Endpoint impact (ou Endpoint) fait référence à un impact à la fin de la chaîne des causes et des effets. Il reflète le dommage ou le bénéfice potentiel (Jorgensen et al, 2008).

***Effet d'impact potentiels***
Le passage de variation d'effets d'impacts potentiels à des variations d'effets réels de capacité dépend des conditions socio-économiques du contexte présidant à l'utilisation des capacités potentielles effectives mises à disposition. Ces contraintes font qu'il n'y a pas automaticité entre un effet potentiel effectif et un effet réel.

***Effet rebond***
En termes simples, l'effet rebond est un concept de la théorie économique classique hérité de Williams Jevons. À la fin du XIXe siècle, Jevons a observé qu'avec l'arrivée de la machine à vapeur de Watt, plus efficace que celle de Newcomen, la consommation de charbon a augmenté au lieu de diminuer. Il a mis en évidence le fait que les améliorations techniques qui rendent l'utilisation d'une ressource plus efficace ont tendance à augmenter sa consommation plutôt qu'à la réduire. La ressource devient moins chère, plus abordable et donc plus largement utilisée. Ce concept a été relancé dans les années 1980 par les travaux de Khazzom et Brookes. Après les deux crises pétrolières, ils ont observé que, dans une même économie nationale, alors que le nombre de voitures plus efficaces augmentait rapidement, la consommation globale d'énergie des voitures continuait d'augmenter. Khazzoom estime que les gains d'efficacité énergétique par unité (à l'échelle micro) entraînent une augmentation de la consommation d'énergie à l'échelle macroscopique. Ce principe a été corroboré par Harty Saunders en 1992, qui énumère trois mécanismes par lesquels une augmentation de l'efficacité énergétique (EE) accroît la consommation totale d'énergie :
- l'augmentation de l'EE rend l'utilisation de l'énergie relativement moins chère, ce qui encourage une utilisation accrue de l'énergie.
- l'amélioration de l'EE entraîne une augmentation de la croissance économique, qui à son tour entraîne une augmentation de la consommation d'énergie dans l'ensemble de l'économie.
- l'amélioration de l'efficacité de l'extraction ou de l'utilisation d'une ressource (agissant comme un goulot d'étranglement) multiplie l'utilisation de toutes les technologies, produits et services complémentaires qu'elle limitait.

### I/
***IP - Impact Pathway***
L'évaluation des voies d'impact (IP) est une méthodologie *SLCA* utilisée pour évaluer les effets potentiels de la distribution des bénéfices sur le long terme. Elle s'inspire de la théorie des changements (ToC) et utilise principalement la relation causale pour décrire les interactions entre les variables des*LCI*, les *Midpoints* et les *Endpoints*. 
Cette méthodologie nécessite des connaissances approfondies et une grande quantité de données.

***Impact transfer***
Un transfert d'impact s'est produit lorsqu'un changement/une décision est bénéfique pour un point spécifique du *SLC*, mais affecte négativement d'autres points du *SLC*.

***Impact social***
L'impact social est le résultat des activités/comportements des organisations liées au cycle de vie du produit/processus/service et de l'utilisation du produit lui-même. Les impacts sociaux sont souvent basés sur plus d'une variable (de *SLC*).
L'impact social peut être un impact social potentiel ou un impact social effectif.

### L/
***LCA - Life Cycle Analysis***
L'analyse du cycle de vie (ou évaluation du cycle de vie) est une méthodologie permettant d'évaluer les impacts associés à toutes les étapes du cycle de vie d'un produit, d'un processus ou d'un service commercial. L'ACV est souvent utilisée pour l'analyse du cycle de vie environnemental (*ELCA*) en raison de la jeunesse de l'ACV social et de la distinction entre *ELCA* et *SLCA*.

***LEX - Life Expectancy at birth***
Un AoP couramment utilisé. L'espérance de vie à la naissance est basée sur une estimation de l'âge moyen que les membres d'un groupe de population donné auront à leur mort.
### M/
***Midpoint Impact***
Un Midpoint Impact (ou Midpoint) fait référence à un impact social situé au milieu de la chaîne des causes et des effets.

***Modèle de valeur***
Le modèle de valeurs sert de représentation pour les principes, l'éthique, la responsabilité et l'ancrage que l'on souhaite donner au projet. Ces modèles sont plus ou moins subjectifs et complet. Le modèle de valeurs traduit une ou des aires de protections (AoP) qui sont intrinsèquement liées à une amélioration de la qualité de vie d'un groupe social identifié. Ainsi, le modèle de valeurs vient servir de support à l'agrégation des indicateurs Endpoints issus de l'évaluation des pathways.

### R/
***RSE - Responsabilités Sociétales des Entreprises***
La responsabilité sociétale des entreprises (RSE) se définit comme **la contribution volontaire des entreprises aux enjeux du développement durable, aussi bien dans leurs activités que dans leurs interactions avec leurs parties prenantes**. Elle concerne trois domaines : environnemental, social et sociétal.

***Risques sociaux***
Généralement déterminés par une ACV sociale avec une méthodologie d'échelle de référence (RS), les risques sociaux mettent l'accent sur la performance sociale d'un produit, d'un processus ou d'un service. Ils reflètent la situation actuelle.

***RS - Reference Scale***
L'évaluation de l'échelle de référence (ER) est principalement axée sur la contribution sociale et correspond à la situation actuelle ou à une vision à court terme. Pour dire les choses crûment, il s'agit d'une performance sociale et non d'une analyse anticipative, qui n'estime donc pas les éventuels *retours de flamme*, *transferts d'impact* et *effets de rebond*.



### S/

***SLC - Social Life Cycle***
Le cycle de vie social (SLC) d'un produit, d'un processus ou d'un service représente tous les groupes de parties prenantes qui sont marginalement influencés par le produit, le processus ou le service, ainsi que les différentes interactions qui les relient (telles que les flux de matériaux et d'immatériels).
Il diffère du cycle de vie environnemental utilisé dans l'E-LCA en raison de la différence de nature des interactions entre l'objet étudié et ses parties prenantes.

***STG - Sustainable Development Goals***
Les dix-sept objectifs sociaux de développement durable acceptés par tous les membres des Nations unies. 

![img](https://paris21.org/sites/default/files/inline-images/sdgs_0.png)



### U/
***UNEP - United Nation Environmental Program***
Le Programme des Nations unies pour l'environnement (PNUE) est une organisation subsidiaire des Nations unies, créée en 1972 pour : coordonner les activités des Nations unies dans le domaine de l'environnement ; aider les pays à mettre en œuvre des politiques environnementales.
