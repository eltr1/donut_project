# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Global Impact Donut (GIDo)'
copyright = 'CC-BY-SA'
author = 'Guilwen Meunier'
release = '2023'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration
import os
import sys

sys.path.insert(0, os.path.abspath('../..'))
sys.path.insert(0, os.path.abspath('../../Code'))

extensions = [
    'sphinx_rtd_theme',
    'myst_parser',
    'sphinx.ext.autodoc'
    ]

templates_path = ['_templates']
exclude_patterns = [
    'requirements.txt'
]
html_static_path = ['Documentation/doc/Images']
language = 'fr'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output
html_static_path = ['Documentation/doc/Images']
html_css_files = [
    'css/custom.css',
]


source_suffix = {
    '.rst': 'restructuredtext',
    '.txt': 'restructuredtext',
    '.md': 'markdown',
}
