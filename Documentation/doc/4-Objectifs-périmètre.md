# 4/ Objectifs périmètre

## Résumé:

L'étape *Objectif & Périmètre* a plusieurs but:
- Amener les parties prenantes à une meilleure compréhension de leur projet, en adoptant un point de vue systémique et multicritère.
- Déterminer les périmètres contexto-spatio-temporel du projet.
- Préparer et orienter la récolte d'informations vers le plus de pertinence et de justesse possible.

Cette étape demande à l'équipe de travailler en appliquant des outils de design systémique afin d'appréhender la complexité du système, puis d'établir les périmètres, et enfin de délimiter le périmètre d'impact.

---

## 0/ Introduction

Dans cette partie, nous chercherons à clarifier des points théoriques qui sont nécessaires à l'évaluation d'impacts multicritères.  En effet, rappelons que le périmètre et les échelles d'impacts ne sont pas identique à ceux du d'une analyse environnementale. Par exemple, en ACV environnementale, les parties impactées non marginales se rapproche des parties prenantes de la chaine de valeur de l'objet. En analyse d'impact sociaux, les parties impactées non marginales prennent aussi en compte le territoire local, les concurrents direct, les consommateurs…

Il convient de ne pas mélanger ces deux méthodologies. Et surtout, de ne pas oublier des parties du cycle de vie social, qui est beaucoup moins habituel. Pour éviter cela, plusieurs informations sont à avoir en tête :
- Qui sont les parties impactées sensibles ?
- Quel périmètre d'impacts ?
- Quel niveau d'impact ?
- Quel valeur d'information ?

Il est donc primordial de bien connaitre l'environnement dans lequel va évoluer le projet. Cette première étape, dites **Périmètre et données** a pour but d'amener les parties prenantes à une meilleure compréhension de leur projet, pour une meilleure application de la méthodologie développé pour l'application Donut. Puisque l'on ne peut pas évaluer un projet sans récolter de données, et qu'on ne peut récolter de données sans définir les périmètres, parties sensibles, biais de subjectivités du projet, il convient d'utiliser des outils pour déterminer chacun des points précédents.

Dans cette partie, nous proposerons un positionnement théorique sur les études et connaissances nécessaires au bon usage de l'application Donut. Nous fournirons aussi plusieurs outils de design systémique[^1] afin d'arriver à déterminer le périmètre et scope d'un projet, et aider à orienter la récolte d'information vers le plus de pertinence et de justesse possible.

---

## 1/ Principes et axiomes

### 1.1 Attention aux transferts d'impacts

L'étude des impacts sociaux impose d'étudier un système complexe [^2]. Un tel système est soumis au **Principe de causalité en boucle**. Chaque élément est à la fois cause et conséquence. On distingue les rétroactions positives (déviance, effet cascade) et négatives (régulatrice).

Les méthodes d'évaluation d'impacts sociaux peuvent être divisées en deux catégories (Sierra et al, 2017):
- La première, appelée évaluation de l'échelle de référence, est principalement axée sur la contribution sociale et correspond à la situation actuelle ou à une vision à court terme. En d'autres termes, il s'agit d'une performance sociale et non d'une analyse anticipative, ce qui ne permet pas d'estimer les transferts d'impact et les boucles de rétroactions.
- La deuxième catégorie est l'évaluation dite du chemin de l'impact (IP - Impact pathway), qui vise à évaluer les effets potentiels de la distribution des bénéfices sur le long terme et utilise des relations de causalité inspirées de la théorie des changements. Cette deuxième catégorie nécessite beaucoup plus de données et de recherches pour identifier les voies reliant les variables et les impacts à mi-parcours.

Prendre en compte les boucles de rétroactions et les transferts d'impacts est nécessaire si l'on considère un système complexe et que l'on souhaite anticiper de manière dynamique les impacts futurs. Il convient de faire le choix d'une approche IP des chemins d'impact [^3].

Dans la version alpha de l'outil (Aout 2023), le choix d'intégrer les boucles de rétroactions indépendamment pour chacun des chemins s'est imposé par finalité de l'application : proposer une méthode d'agrégation et de comparaison de scénario, avec une finesse et justesse élevée mais sans modèle causale complet. L'influence des boucles rétroactions est à prendre en compte dans la définition des chemins, et lorsque la subjectivité est de mise, dans leur évaluation.  Il est donc nécessaire d'avoir en amont une bonne connaissance des tendances et liens causaux du système.

### 1.2 Axiome de la relativité

Les méthodologies d'évaluation d'impacts sociaux sont plus complexes que celles pour les impacts environnementaux : les catégories sociales ajoutées sont de nature à la fois quantitative et qualitative, voire parfois semi-quantitative. Ces différentes natures ne peuvent pas être agrégées directement ensemble car le résultat manquerait de consistance et de cohérence.

Une analyse multicritère avec du social doit donc analyser chaque indicateur indépendamment, soit en limitant l'étude à un seul point final, soit en obtenant des valeurs relatives par comparaison avec un état de référence. Ces valeurs relatives reflètent l'impact potentiel qui peut se produire par rapport à un état de référence pour chaque partie prenante et chaque capitale.

Ainsi si l'on compare deux scénarios entre eux, la méthode doit s'assurer de la cohérence des unités fonctionnelle et de la consistance de leur comparaison.

### 1.3 Axiome de la Subjectivité

Le système étudié doit respecter le **principe de subjectivité** : En construisant des catégories de valeurs sociales, il est recommandé de couvrir au moins des sous-catégories spécifiques qui sont jugées pertinentes par les experts et la littérature. Il est nécessaire de s'assurer que le choix des critères de l'analyse n'est pas orienté à des fins de marketing. Cependant, le respect de ce principe est plus exigeant en termes de données.

Le respect de cet axiome peut être assurer en imposant des valeurs "sensibles" en fonction de type de projet qui est évalué.

### 1.4 Axiome de la diversité

Le système étudié doit respecter le **principe de diversité**. D'une part, l'analyse d'impacts (sociaux) est influencée par le contexte et l'étude doit donc être spécifique au cas et au contexte. D'autre part, les indicateurs sociaux doivent être universels.

Cela nécessite une réflexion conséquente sur le modèle de valeur associé à la méthodologie.

---

## 2/ Périmètres d'impacts

### 2.1 Parties impactées sensibles

Il existe un consensus sur le fait qu'un cycle de vie social complet doit prendre en compte 6 parties prenantes différentes[^4] :
- Les consommateurs
- La communauté locale
- Les travailleurs
- Acteurs de la chaîne de valeur
- Société
- Les enfants

Ainsi, pour chacun des 6 catégories, il convient de déterminer les groupes sensibles aux impacts du projet. Un groupe est sensible si les impacts sont non marginaux. Pour cela, des outils de design tels la Gigamap, la boite à outil de cartographie du système, l'enquête itérative sont intéressants.

### 2.2 Périmètres d'impact

Il est nécessaire d'identifier le périmètre d'impact, puis de regarder ses déclinaisons avec chacun groupes sensibles. Afin de comprendre la richesse des variables qui impactent les parties prenantes sensibles, il est intéressant de scinder le périmètre d'impact en plusieurs périmètres. La méthode Symbiosis in Development [^5] propose trois dimensions pour le périmètre :
- Le temps
- L'espace
- Le contexte

Cette représentation est pertinente car elle reflète les trois natures des informations qui sont à relever :
- informations de localisation (climat, agriculture, sols, ressources naturelles...)
- informations temporelles (efforts de planification déjà prévu, données historiques utilisables, rapports annuels, politiques gouvernementales...)
- informations contextuelles (complexité du système, parties prenantes, nature des structures, situation géopolitique...)

Ainsi nous trouvons pertinent d'étudier ces 3 périmètres dans la première étape **Objectifs et périmètres**. Cela peut se faire à l'aide d'outil de design systémique comme les Gigamap, la cartographie de contexte la cartographie des tendances.

### 2.3 Proposition d'outils pour l'étape **Objectifs et périmètres**

Pour déterminer les périmètres ainsi que comprendre le système et ses liens causaux (afin de choisir un modèle de valeur, des indicateurs adaptés), il est nécessaire d'interagir avec les parties prenantes. Le design, en particulier systémique, est alors un atout de taille dans la discussion, et l'organisation des échanges. Ces outils de design se réalisent sous la forme d'ateliers participatifs animés par un designer-facilitateur connaissant les outils et la finalité de l'exercice.

Les quelques exemples cités précédemment sont justement à titre d'exemple. Ces outils de design systémique sont souvent intéressants s'ils sont combinés. La principale contrainte est alors combien de temps est-il possible de libérer pour cette étape.

A titre d'exemple, ce tableau propose trois "menus" d'outils afin d'établir de manière plus ou moins rapide une connaissance approfondit des périmètres et enjeux.

![Outils design systémique](Images/Scope_outil_syst.png)
<p align="center"><i> Tableau 1: Menus d'outils pour définir périmètres et objectifs </i> </p>

Les outils de design mentionnés dans les "Menus" sont détaillés dans ces tableaux :

![Outils design systémique](Images/Tableau_outil_perimetre.png)
<p align="center"><i> Tableau 2: Les outils liés aux périmètres </i> </p>

![Outils design systémique](Images/Tableau_outil_indicateurs.png)
<p align="center"><i> Tableau 3: Les outils liés aux indicateurs </i> </p>

Prendre connaissance de ces outils est toujours enrichissant, il n'est que recommandé d'en appliquer certains afin d'élaborer ou d'améliorer le modèle d'un projet, mais aussi afin d'éviter des écueils et d'oublier quelque chose d'important.



[^1]: Daumal, S., 2023. 58 outils de design systémique, Edition Eyrolles. ed.

[^2]: Un système complexe est constitué de nombreux éléments en interaction dynamique. Le système ne peut être réduit à la somme de ses parties (ex: corps humain, climat...). Sa complexité tient à son caractère d'émergence spontanée qui rend difficile sa compréhension et apprehension. Quelques principes clés qui caractérisent un système complexe: 1. **Principe d'émergence**: Le système a des qualités absentes des composants du tout. Elles "emergent" quand on considère le tout et pas seulement une partie. // 2. **Principe dialogique**: relation à la fois complémentaire et antagoniste entre deux caractères. Par exemple la vie n'existe que par opposition à l'état de mort. // 3. **Principe hologrammique**: Le tout est présent dans chaque partie qui le compose (ex: l'ADN est dans chaque cellule, la notion de société est porté par chaque individu qui la compose...) // 4. **Principe de causalité en boucle**: Abandon de la causalité linéaire. Prend en compte des rétroactions. Chaque élément est à la fois cause et conséquence. On distingue les rétroactions positives (déviance, effet cascade) et négatives (régulatrice).

[^3]: L'évaluation des voies d'impact (IP) est une méthodologie *SLCA* utilisée pour évaluer les effets potentiels de la distribution des bénéfices sur le long terme. Elle s'inspire de la théorie du changement (ToC) et utilise principalement la relation causale pour décrire les interactions entre les variables de l'*LCI*, les *Midpoints* et les *Endpoints*. 

[^4]: Guidelines for Social Life Cycle Assessment (S-LCA) - Life Cycle Initiative, 2023. 

[^5]: Symbiosis in Development (SiD) [WWW Document], n.d. URL [https://except.eco/knowledge/symbiosis-development-sid/](https://except.eco/knowledge/symbiosis-development-sid/) (accessed 6.29.23).
