## **Bibliothèque ACV sociale**

1. ACV sociales: Effet socio-économique des chaînes de valeurs. -Lien vers livre (2023). Available at: https://www.fruitrop.com/media/Publications/FruiTrop-Thema/ACV-Sociale-volume-1 (Accessed: 28 July 2023).
2. ‘Capabilité’ (2023) Wikipédia. Available at: https://fr.wikipedia.org/w/index.php?title=Capabilit%C3%A9&oldid=204968890 (Accessed: 14 August 2023).
3. CSTB (2023) Ecoscale, CSTB Évaluation. Available at: http://evaluation.cstb.fr/fr/ecoscale/ (Accessed: 29 June 2023).
4. ‘Guidelines for Social Life Cycle Assessment (S-LCA) - Life Cycle Initiative’ (2023), 15 February. Available at: https://www.lifecycleinitiative.org/activities/life-cycle-assessment-data-and-methods/global-guidance-for-life-cycle-impact-assessment-indicators-and-methods-glam/guidelines-for-social-life-cycle-assessment-solca/ (Accessed: 29 June 2023).
5. Joffe, H. (2007) ‘Le pouvoir de l’image : persuasion, émotion et identification’, Diogène, 217(1), pp. 102–115. Available at: https://doi.org/10.3917/dio.217.0102.
6. Jolliet, O. et al. (2003) ‘IMPACT 2002+: A new life cycle impact assessment methodology’, The International Journal of Life Cycle Assessment, 8(6), p. 324. Available at: https://doi.org/10.1007/BF02978505.
7. Macombe, C. et al. (2013) ACV sociales: Effet socio-économique des chaînes de valeurs. Montpellier.
8. ‘Methodological Sheets for Subcategories in Social Life Cycle Assessment (S-LCA) 2021 - Life Cycle Initiative’ (2021), 6 December. Available at: https://www.lifecycleinitiative.org/library/methodological-sheets-for-subcategories-in-social-life-cycle-assessment-s-lca-2021/ (Accessed: 29 June 2023).
9. Objectifs de développement durable (2023). Available at: https://www.un.org/sustainabledevelopment/fr/objectifs-de-developpement-durable/ (Accessed: 29 June 2023).
10. Social LCA, volume 4 - 6th SocSem (2023). Available at: https://www.fruitrop.com/media/Publications/FruiTrop-Thema/Social-LCA-volume-4-6th-SocSem (Accessed: 29 June 2023).
11. Social LCA, volume 5 - 7th SocSem (2023). Available at: https://www.fruitrop.com/media/Publications/FruiTrop-Thema/Social-LCA-volume-5-7th-SocSem (Accessed: 29 June 2023).
12. Synthèse et analyse du nouveau rapport du GIEC (2021) Bon Pote. Available at: https://bonpote.com/synthese-et-analyse-du-nouveau-rapport-du-giec/ (Accessed: 14 August 2023).
13. The Green Guide Explained : BRE Group (2023). Available at: https://tools.bregroup.com/greenguide/page.jsp?id=3612 (Accessed: 29 June 2023).


## Bibliothèque Systémique

14. Braun, W. (2002a) ‘System Archetypes’.
15. Braun, W. (2002b) ‘The System Archetypes The System Archetypes’, System, 2002.
16. Causal Layered Analysis Defined - ProQuest (2023). Available at: https://www.proquest.com/openview/5278721596e028a9a8e88a314b4d0e54/1?pq-origsite=gscholar&cbl=47758 (Accessed: 29 June 2023).
17. Daumal, S. (2023) 58 outils de design systémique. Edition Eyrolles.
18. Designing powerful questions | Conversational Leadership (2023). Available at: https://conversational-leadership.net/powerful-questions/ (Accessed: 3 July 2023).
19. dfitzgerald (2017) ‘Systems Thinking Toolkit’, FSG, 8 June. Available at: https://www.fsg.org/resource/systems-thinking-toolkit-0/ (Accessed: 4 July 2023).
20. ‘Download the data | Gapminder’ (2023), 29 June. Available at: https://www.gapminder.org/data/ (Accessed: 29 June 2023).
21. Easterbrook, S. (no date) ‘Guidelines for Drawing Causal Loop Diagrams’.
22. Geels, F.W. and Schot, J. (2007) ‘Typology of sociotechnical transition pathways’, Research Policy, 36(3), pp. 399–417. Available at: https://doi.org/10.1016/j.respol.2007.01.003.
23. Goodman, M. (2016) Using the Archetype Family Tree as a Diagnostic Tool, The Systems Thinker. Available at: https://thesystemsthinker.com/using-the-archetype-family-tree-as-a-diagnostic-tool/ (Accessed: 29 June 2023).
24. How to create and use a causal diagram (DAG) (2022) Causal Diagrams. Available at: https://causaldiagrams.org/guides-and-tutorials/how-to-create-and-use-a-causal-diagram-dag/ (Accessed: 29 June 2023).
25. Jones, P. (2017) ‘Social Ecologies of Flourishing: Designing Conditions that Sustain Culture.’, in, pp. 38–54. Available at: https://doi.org/10.4324/9781315229065-4.
26. Lannon, C. (2016) Causal Loop Construction: The Basics, The Systems Thinker. Available at: https://thesystemsthinker.com/causal-loop-construction-the-basics/ (Accessed: 29 June 2023).
27. Leverage Points Canvas Walkthrough | Articles (2023) Si Network. Available at: https://www.systemsinnovation.network/posts/22290145 (Accessed: 4 July 2023).
28. ‘Leverage Points: Places to Intervene in a System’ (2023) The Academy for Systems Change, 3 July. Available at: https://donellameadows.org/archives/leverage-points-places-to-intervene-in-a-system/ (Accessed: 3 July 2023).
29. LOOPY! (2023). Available at: http://ncase.me/loopy/ (Accessed: 29 June 2023).
30. Loye, D. (2006) Measuring Evolution. David Loye.
31. Sevaldson, B. (2022) Library of Systemic Relations, Systems Oriented Design. Available at: https://systemsorienteddesign.net/library-of-systemic-relations/ (Accessed: 29 June 2023).
32. Symbiosis in Development (SiD) (2023). Available at: https://except.eco/knowledge/symbiosis-development-sid/ (Accessed: 29 June 2023).
33. The Design Value Framework - Design Council (2023). Available at: https://www.designcouncil.org.uk/our-resources/the-design-value-framework/ (Accessed: 29 June 2023).
34. The Tarot Cards Of Tech (2023). Available at: https://tarotcardsoftech.artefactgroup.com/ (Accessed: 3 July 2023).

## Bibliothèque documentation


35. A beginner’s guide to writing documentation (2023) Write the Docs. Available at: https://www.writethedocs.org/guide/writing/beginners-guide-to-docs/guide/writing/beginners-guide-to-docs/ (Accessed: 5 July 2023).
36. Introduction à Sphinx, un outil de documentation puissant (00:00:00+02:00) FLOZz’ Blog. Available at: https://blog.flozz.fr/2020/09/07/introduction-a-sphinx-un-outil-de-documentation-puissant/ (Accessed: 5 July 2023).
37. Marp Web (under construction) (2023). Available at: https://web.marp.app/ (Accessed: 15 August 2023).
38. Petrov, A. (2014) ‘How to maintain a successful open source project’, Code Zen, 29 April. Available at: https://medium.com/code-zen/how-to-maintain-a-successful-open-source-project-aaa2a5437d3a (Accessed: 5 July 2023).
39. Verchère, R. (2021) ‘Making slides from anywhere for anyone using Marp, GitLab Pages and Gitpod’, LINKBYNET, 9 September. Available at: https://medium.com/linkbynet/making-slides-from-anywhere-for-anyone-using-marp-gitlab-pages-and-gitpod-35001daf1c93 (Accessed: 15 August 2023).

## Bibliothèque Mesure d'impact


40. Base Empreinte® (2023). Available at: https://base-empreinte.ademe.fr/ (Accessed: 4 July 2023).
41. Dit", A.D.P. "Aussitôt (2020) ‘La crise écologique comme crise de la sensibilité/Baptiste Morizot/ Texte n°34’, aussitôt dit, 17 August. Available at: https://aussitotdit.net/2020/08/17/la-crise-ecologique-comme-crise-de-la-sensibilite-baptiste-morizot-texte-n36/ (Accessed: 14 August 2023).
42. Elioth (2023) Paris change d’ère. Available at: http://paris2050.elioth.com/ (Accessed: 29 June 2023).
43. Enquête nationale transports et déplacements (ENTD) 2008 | Données et études statistiques (2023). Available at: https://www.statistiques.developpement-durable.gouv.fr/enquete-nationale-transports-et-deplacements-entd-2008 (Accessed: 29 June 2023).
44. Évaluation qualitative et réflexive a priori (2023). Available at: https://stph.librecours.net/modules/lownum/low03/web/co/low03.html (Accessed: 29 June 2023).
45. Évaluer l’empreinte environnementale d’un·e habitant·e avec Empreinte_Regionale | Observatoire de l’environnement en Bretagne (2023). Available at: https://bretagne-environnement.fr/evaluer-empreinte-region-environnement-outil (Accessed: 29 June 2023).
46. Indicateurs pour le suivi national des objectifs de développement durable | Insee (2023). Available at: https://www.insee.fr/fr/statistiques/2654964 (Accessed: 7 August 2023).
47. Individus localisés au canton-ou-ville en 2016 − Logements, individus, activité, mobilités scolaires et professionnelles, migrations résidentielles en 2016 | Insee (2023). Available at: https://www.insee.fr/fr/statistiques/4229118?sommaire=4171558 (Accessed: 29 June 2023).
48. La rationalité de l’homo economicus : une hypothèse infondée - Fiche - The Other Economy (2023). Available at: https://theothereconomy.com/fr/fiches/la-rationalite-de-lhomo-economicus-une-hypothese-infondee/ (Accessed: 14 August 2023).
49. Le Monde.fr (2012) ‘Le 7e continent de plastique : ces tourbillons de déchets dans les océans’, 9 May. Available at: https://www.lemonde.fr/planete/article/2012/05/09/le-7e-continent-de-plastique-ces-tourbillons-de-dechets-dans-les-oceans_1696072_3244.html (Accessed: 14 August 2023).
50. Les dépenses des ménages en France métropolitaine en 2011 − Les dépenses des ménages en 2011 | Insee (2023). Available at: https://www.insee.fr/fr/statistiques/2015662?sommaire=2015691#consulter (Accessed: 29 June 2023).
51. Méthode QuantiGES (2023) La librairie ADEME. Available at: https://librairie.ademe.fr/changement-climatique-et-energie/4827-methode-quantiges-9791029718236.html (Accessed: 3 July 2023).
52. SDG Indicators — SDG Indicators (2023). Available at: https://unstats.un.org/sdgs/indicators/indicators-list/ (Accessed: 7 August 2023).
53. ‘SiD Sustainability Definition’ (2017) Symbiosis in Development (SiD), 22 February. Available at: https://thinksid.org/?p=125 (Accessed: 7 August 2023).
54. Synthèse et analyse du nouveau rapport du GIEC (2021) Bon Pote. Available at: https://bonpote.com/synthese-et-analyse-du-nouveau-rapport-du-giec/ (Accessed: 14 August 2023).
55. UNSDG (2023). Available at: https://unstats.un.org/sdgs/dataportal (Accessed: 7 August 2023).
56. Urvoy J., Gontier L, MEUNIER G (2022) Du mètre carré à l’usager : Passer de l’ACV du bâtiment à l’empreinte carbone de ses habitants - Elioth, Elioth by Egis. Available at: https://elioth.com/du-metre-carre-a-lusager-passer-de-lacv-du-batiment-a-lempreinte-carbone-de-ses-habitants/ (Accessed: 29 June 2023).
## Bibliothèque Résilience

57. Bihouix, P., Jeantet, S. and De Selva, C. (2022) La ville stationnaire: comment mettre fin à l’étalement urbain ? Edition Domaine du possible. Acte Sud.
58. (2022) Manifeste pour une Frugalité heureuse et créative, Frugalité heureuse & créative. Available at: https://frugalite.org/manifeste/ (Accessed: 29 June 2023).
59. ‘Les scénarios’ (2023) Agence de la transition écologique, 29 June. Available at: https://www.ademe.fr/les-futurs-en-transition/les-scenarios/ (Accessed: 29 June 2023).
60. Logement - Logements par typologie et période de construction Pays de la Loire : TerriSTORY, outil d’aide à la transition des territoires (2023). Available at: https://teo-paysdelaloire.terristory.fr/?zone=region&maille=epci&zone_id=52&analysis=3648&theme=Logement&nom_territoire=Pays%20de%20la%20Loire (Accessed: 29 June 2023).
61. Planetary boundaries data - Stockholm Resilience Centre (2023). Available at: https://www.stockholmresilience.org/research/planetary-boundaries/planetary-boundaries-data.html (Accessed: 11 July 2023).
62. Raworth, K. (2017) Doughnut Economics Seven Ways to Think Like a 21st-Century Economist. (1 vol). Random House Business Books.
63. SankeyDiagram.net: Generate Sankey Diagrams with ease (2023). Available at: https://sankeydiagram.net (Accessed: 29 June 2023).
64. Scénario négaWatt 2022 (2023) Association négaWatt. Available at: https://negawatt.org/Scenario-negaWatt-2022 (Accessed: 29 June 2023).
65. SIMAY, C. and SIMAY, P. (2022) La Ferme Du Rail. Edition Domaine du possible. Actes Sud.
66. Sinamet - Système d’Information pour l’Analyse du Métabolisme des Territoires — Résilience des Territoires (2023). Available at: https://wiki.resilience-territoire.ademe.fr/wiki/Sinamet_-_Syst%C3%A8me_d%27Information_pour_l%27Analyse_du_M%C3%A9tabolisme_des_Territoires (Accessed: 29 June 2023).
67. Tools | DEAL (2023). Available at: https://doughnuteconomics.org/tools?page=1 (Accessed: 11 July 2023).
68. Traverse, L. (2023) Fiches d’action pour la résilience – La Traverse. Available at: https://la-traverse.org/fiches-action-resilience (Accessed: 29 June 2023).
69. Useful Resources - A Good Life For All Within Planetary Boundaries (2023). Available at: https://goodlife.leeds.ac.uk/related-research/useful-resources/ (Accessed: 11 July 2023).

