# 1/ Contexte et génèse

## 1/ Contexte

### 1.1 Situation environnementale

##### a/ Réchauffement climatique global.

Les moments de canicules et de sécheresses s'intensifient en France et partout dans le monde, de même que les incendies, ce sont là les causes directes d'un réchauffement climatique de la surface du globe. Cette notion de réchauffement est l'image la plus répandue pour parler du changement climatique. C'est en effet la plus simple à mesurer : son potentiel est traduit par le PRG (*Potentiel de Réchauffement Global*) que l'on peut calculer facilement grâce à un calcul des émissions des gaz à effets de serres liés à notre activité anthropique.

Rappelons en effet que les travaux du groupe d'expert pour le climat GIEC[^1] ont montré avec plus d'une vingtaine d'année d'accumulation de preuves scientifiques que l'activité humaine est la principale contrainte s'exerçant sur notre environnement, et par conséquent la cause de cette dégradation climatique. Il y a consensus scientifique sur la nécessité de modifier nos modes de vies et de production pour maintenir des zones habitables sur terre. L’ONU déplore que les émissions de gaz à effet de serre stagnent à 52 Gt annuels, alors qu’il faudrait les limiter à 36, voire 24Gt, pour rester en dessous des 2 °C comme stipulé comme objectif mondial lors de la COP 21. La COP 23 affiche son impuissance : les engagements pris lors de la COP 21, en 2016, conduisent à une hausse de plus 3 à 3,5 °C.

Le réchauffement climatique est une bonne entrée en matière, mais c'est loin d'être le seul impact préoccupant.

![climat réchauffement](Images/1_climat.PNG)
<p align="center">Fig SPM 5</p>

##### b/ Crise des ressources

Notre mode de consommation linéaire, extractiviste, propre à une mentalité de croissance infini, ne prend pas en compte les ressources minières ni le vivant comme un capital dont il faut utiliser avec discernement. L'économie néolibéraliste du XXe siècle (dont le XXIe à hérité) les considèrent comme un revenu, dans lequel nous pouvons puiser sans se soucier des conséquences. Le risque n'est pas tant le manque immédiat, puisqu’au vu des politiques d'économies circulaires et des avancées de la techniques, la société arrivera sûrement à repousser certaines crises de ressources. Le risque principal est implicite : la raréfaction des localisations des ressources promet des tensions géopolitiques accrues. Elles se traduisent d’ores et déjà : tensions sur le gaz russe, hausse du prix des matières premières, hausse des prix de l’énergie, crise des semi-conducteurs.

##### c/ Crise de la biodiversité

L'IPBES (*équivalent du GIEC pour la biodiversité*) alerte sur le désastre écologique qu'entraine les activités humaines[^2]. Quelques-uns des impacts les plus graves sont:
- Pollution des sols, des mers, de l'air
- Acidification des océans
- Disparition de 50% des grands mammifères, des oiseaux, des insectes participant au cycle naturel
- La Terre devient une déchetterie mondiale (7e continent dans l'océan pacifique constitué de plastique [^4])
- Pertes des capacités de stockage carbone des sols, pertes de leur résilience aux aléas climatiques extrêmes, perte de productivité des sols...

### 1.2 A la racine : une société au bord du gouffre ?

Il ne faut cependant pas traiter cette crise "des ressources et de la biodiversité" comme un cas isolé. Cela voudrait dire résumer le problème à une orientation technique de notre production (*portant sur la consommation énergétique et la gestion des ressources*). Il faut prendre du recul pour comprendre l'origine des choix qui ont induit cette crise écologique.

##### a/ Le XXe siècle et la croissance

La révolution industrielle du XIXème siècle a fait basculer une société à dominante agraire et artisanale vers un monde industriel et commercial. Cet essor industriel s'est accompagné d'une hausse du niveau de vie et des revenus importantes. De nombreux penseurs, établir des modèles économiques et macro-économique pour caractériser cette effervescence nouvelle de flux et de ressources. Nous étions en train de passer d'une économie d'état-nation à état-monde.

Cette époque fut le moment d'une bascule sans précédent de la définition d'économie adopté par la société. En 1844, John Stuart Mill définit celle-ci comme les lois qui apparaissent derrière notre recherche de richesse [^5]. Cela conduit les économistes à modéliser la nature humaine lorsqu'un individu s'exprime sur un marché théorique : ce fut la naissance de l'homo eoconomicus[^6]. Par la même occasion ce modèle justifia une certaine rigueur et théorie qui présentèrent alors l'économie comme une science, c'est à dire comme une discipline recherchant la vérité, et dotée d'axiomes profonds auxquels aucun individu n'échappe. Autrement dit le modèle de Adam Smith complété par John Stuart Smith fut utilisé par leurs successeurs, sans prendre en compte les limites du modèle, pour arriver à la conclusion suivante : "Les conclusions qui découlent de la science économique sont indiscutables, au même titre qu'une pomme tombe d'un arbre sous l'effet de la gravité, le bien-être se traduit par la croissance économique".

Le PIB introduit par Kuznet [^7] et le schéma flux circulaire de Samuelson [^8] très vite accepté et adopté par leur puissance visuelle et leur simplicité à se mettre en place, propulsère la recherche de croissance économique au niveau d'indicateur principal des politiques. Ces outils, ne reflètent plus une macro-économie au service de la société. Le PIB traduit le niveau de progrès économique d'un pays, mais pas son niveau de vie.  Quant au schéma flux circulaire, il ne fait ni apparaître les ressources et énergies nécessaire à son fonctionnement, ni la société qui doit récupérer le progrès et le fruit de l'économie. Cette rupture du réel avec le monde économique s'exprime particulièrement à la sortie de la Guerre du Vietnam, le modèle d'entreprise de l'Ecole de Chicago s’impose :  le marché est auto-suffisant et les entreprises ont comme objectif de faire du profit.

##### b/ La croissance économique est la solution à ses problèmes

###### Les inégalités

La courbe de Kuznet[^9] (Avec la croissance, les inégalités vont d'abord augmenter avant de diminuer) théorisait que "la croissance aplanirai les inégalités". Cependant, ces conjectures furent testées dans les années 1990 : la tendance est qu'il n'y a pas de tendance. Tout est possible, et les conjectures sont donc fausses. En fait la conjecture de Kuznet était valide dans son contexte : où le gouvernement atténuait la rétroaction de la Victoire aux victorieux (contexte d'après-guerre). Ce n'est plus le cas aujourd'hui. Le modèle de Kuznet ne fonctionne pas car les règles du jeu ont changé.

Ce dérèglement traduit le profond problème de répartition des ressources qui ne fait qu'aggraver les inégalités sociales. Le modèle économique favorise la taille et la consommation. Ainsi 5% des plus riches possèdent 90% des richesses monétaires mondiales. Les investissements réalisés sont presque essentiellement orientés pour accroître la fortune déjà présente.

De plus, les inégalités ont des impacts importants sur l'écosystème humain :
- Elles augmentent la consommation ostentatoire et la concurrence de statut.
- Elles érodent le capital social.
- Limite la cohésion du tissu social pour agir collectivement (en amenant xénophobie, racisme, individualisme).

Hors selon un étude de Wilkinson et Pickett [^10] , c'est l'inégalité nationale et non la richesse nationale qui influence le plus le bien-être social des nations. Les sociétés égalitaires (qu'elles soient riches ou pauvres) sont plus saines, plus démocratique et plus heureuses.

###### Un drôle de direction technologique

L'essor de la technologie se traduit par deux dynamiques :
- La première est celle de l'ère des réseaux et de la collaboration à coût marginal proche de zéro (technologies latérales à la base du design distributif, on devient prosommateur, fabricant et utilisateur).
- La deuxième est le monopole technique, qui gèrent les communs mondiaux pour leurs propres intérêts commerciaux, s'armant pour garder ce monopole.

On observe aussi ce que l'on nomme "le Grand découplage" ou la productivité croit et l'emploi se casse la figure. Cela vient de l'automatisation et la numérisation qui menacent de nombreux emplois. Le risque ici est que l'autonomisation soit trop rapide pour que la transition des emplois suivent, c'est ce qui se passe et cela se traduit par une économie mal adaptée, et de nombreux emplois dégradants ou des "bullshit jobs". L'augmentation du capital par emploi dû à la machinisation entraîne un degré de compétence plus important pour ces postes. La masse populaire est alors de moins en moins employable.

La volonté de simplifier le travail à l'origine de progrès technique est devenu une volonté d'invisibilisé le travail. On fait des systèmes opaques, à la fois pour garder le secret du fonctionnement, et pour simplifier la tâche à l'utilisateur. Cela résulte en une immense dépendance à nos objets techniques. Ceux-ci modifient nos codes sociaux, notre rapport au travail. Nous sommes une société paresseuse.

Si l'on fait un peu d'histoire de la technique, on observe que la spécialisation accrue des corps d'ingénieurs nécessaire à la complexité technique des ouvrages du XXe siècle les a rendus vulnérable et les a séparé d'une vision holistique et politique de la technique.

###### Un nouveau rapport à l'information

L'effervescence des médias, support de communications ne serait pas si problématique si les algorithmes qui diffusent l'informations ne fonctionnaient pas uniquement sur une économie de l'attention[^11]. Ce nouveau marché, économiquement très intéressant, est terriblement néfaste pour la société plus généralement.

- Cette économie de l'attention est motivée par une économie de la consommation, dont le modèle linéaire altère nos ressources en déchets.
- Les algorithmes, pour capter l'attention, prennent en compte les goûts des utilisateurs : ils diffusent des informations qui alimentent le biais de confirmation des individus.
- La surcharge d'informations contradictoires, partiellement fausses, ou sémantiquement modifiées pour faire le buzz, perd les individus qui ne savent plus quoi croire. Cela alimente notamment encore le biais de confirmation.
- Le captage individualiste de l'attention détourne de réflexions politiques ou holistique : le loisir engendré par la numérisation détruit la vie sociale de proximité, déconnecte les individus de leur réel, et les plonge dans un monde connecté qui demande toujours plus d'attention.

Les théories et arguments scientifiques, de plus en plus utilisés n'importe comment, comme arguments d'autorité par des politiques corrompues par leurs intérêts de croissance, entraine une crédence de plus en plus faible pour le milieu scientifique. Comme ça Donald Trump a pu accuser de Fake News le changement climatique. Cela conduit plus simplement à une distanciation grandissante entre le monde scientifique et technique et la société, ces deux mondes ne se comprennent plus, et leurs intérêts divergents prennent le dessus.

Nous traversons en effet une crise de notre rapport à l'information.

### 1.3 Une crise de nos relations

Le Philosophe Baptiste Morizot  [^12] nous rappelle que cette "crise écologique actuelle, plus qu'une crise des sociétés humaines d'un côté et une crise du vivant de l'autre, est une crise de nos relations au vivant.".

C'est avant tout une crise de nos relations productives aux milieux vivants, visible dans la frénésie extractiviste, linéaire et financiarisé de l'économie politique dominante. Mais c'est aussi une crise de nos relations collectives et existentielles, de nos affiliations au vivant (*qui commande la question de leur importance, et par lequel on les situe dans ou hors de notre monde perceptif, politique*). C'est ainsi une crise éthique.

### 1.4 Une réflexion sur nos organisations territoriales

<p align="center"><i> ***L'aménagement urbain est l'expression de notre organisation sociale. ***</i> </p>

La qualité de l'environnement influence le degré d'appropriation et d'investissement des résidents, et donc la durabilité d'un lieu[^13]. Le défi pour les bâtiments est de trouver un équilibre, une manière de vivre ensemble, une manière de partager l'espace et les ressources entre tous les êtres, humains et non-humains. L'évolution de l'organisation des villes doit prendre en compte cette notion de travail et de vie sociale. Il est urgent de réfléchir à des modes d'organisation et de travail plus durables et plus solidaires.

> C'est en permettant de nouvelles manières de s'organiser que l'on permettra véritablement aux transitions de contrebalancer le conditionnement social engendré notre héritage du XXe siècle.

La volonté politique d'urbanisation des sols suppriment, tous les 10 ans, l’équivalent de la surface d’un département en terres agricoles, les logements vacants représentes 8,3% du parc immobilier, les impératifs de compensations entrent en concurrence avec les projets d'aménagements, les moyens de naturalisation ne font pas consensus si bien que l'on reporte les responsabilités ailleurs. Enfin, les coûts sociaux engendrés ne sont pas pris en compte.

Pour atteindre nos objectifs de transition, pour répondre à nos crises de relations, nous avons besoins d'une nouvelle organisation sociale. Cependant nous sommes bloqués par des villes qui veulent croître à tout prix. Dans son ouvrage *La ville stationnaire*[^14], Philippe Bihouix dresse le portrait des villes actuelles et de leurs problématiques :

- **Le prix du foncier altère les villes-centres en quartier tertiaire et riches**, obligeant de plus en plus de quartiers résidentiels en périphéries et repoussant plus loin les cœurs du métabolismes urbains.

- **La vision majoritaire de densification**, afin de diminuer l'espace par habitant et d'optimiser les transports en communs, poussent à une exploitation du moindre mètre carré de parcelle. On **uniformise alors l'architecture et les bâtiments** et contraint toutes innovations organisationnelles de nos villes. Cela se traduit par la perte d'identité territoriale des bâtiments, malgré les plans d'urbanisme, mais aussi -- et surtout -- par une perte de savoir-faire des artisans, qui ne s'entraînent que sur des techniques "conventionnelles" à base de matériaux bétons/métaux.

- Nous sommes à un **stade de saturation face à la croissance démographique et l'exode rurale**. Cela se traduit par le surencombrement des voiries, des parkings et des embouteillages. Ce phénomène accentue encore la demande sur le foncier et donc l'augmentation de leur prix, par la vieille loi du marché, passant outre la SRU.

- Nous **stagnons alors à un stade de congestion d'environ 30%** (*temps de déplacement supplémentaire lié*). La politique pour s'en affranchir est alors de nouvelles infrastructures pour fluidifier le transport, mais cela résulte à un choix d'habiter plus loin d'une ville repoussante et donc un temps de congestion équivalent.

- Les **Plans locaux d'urbanismes et ceux des territoires sont trop longs à modifier**, ils contraignent les initiatives et les nécessaires choix à effectuer.

Face à ces enjeux, différentes visions émergent. Cependant les visions dominantes (motivés par une idéologie de croissance) ont une forte fermeture : elles détruisent l'émergence d'autres organisations.  Le besoin d'articulation entre ces différentes visions, sous-tendues par des modèles de valeurs différents, est aujourd'hui plus fort que jamais. L'adaptation et la transition doit se faire à toutes les échelles, et chaque échelle doit entreprendre une transition en faisant preuve de subsidiarité et en laissant ouvert les possibles.

**Vers des villes mondes ?**

Faut-il uniformiser dangereusement les villes, dans une course à la "modernité agréable à vivre" et "mondialement compétitive". Les mégalopoles sont les premières à subir l'accélération continue de nos modes de vie liés à cette mondialisation. **La technologie évolue si vite que la société ne s'y adapte pas assez vite**, elle est submergée par tant de multitude de fonction et par un sentiment d'urgence croissant et l'impression de "rater quelque chose".

**Vers des villes intelligentes ?**

Selon Bihouix, Le problème d'une "tout smart city", s'inspirant de The Line ou Songdo, vient du fait qu'elle ne cherche qu'une optimisation : un gain en efficacité et en gestion des ressources. C'est louable mais ses applications sont restreintes et profitent pour l'instant plus aux secteurs économiques de la pub et des jeux vidéo que vers les valeurs de transitions annoncées. **Il n'y a pas de méthodologie pour évaluer les effets rebonds, le écueils et les effets d'inertie du système.** L'énergie grise nécessaire à l'émergence de tels projets est à l'opposé des politiques de réduction des émissions carbones.

Il faut voir le tableau plus large, et faire du techno-discernement. Ce n’est pas le bâtiment qui est intelligent, ce sont ses habitants.

Cependant l'émergence des *smartgrids* et autres réseaux IA intelligents, permettent des économies de ressources et d'énergies phénoménales dans certains cas : l'optimisation et la maintenance de réseaux complexes. Cette complexité pouvant être nouvelle (*par la libéralisation des moyens de production d'électricité par exemple*) ou bien être hérité d'une organisation sociale antérieure (*les réseaux d'eaux et d'égouts en sont un bon exemple :  la croissance des villes et les travaux successifs réalisés complexifient exponentiellement ces réseaux, qui ne sont plus adaptés.*)

Alors, jusqu'à ou tirer parti d'une technologie intelligente pour gérer nos villes ? Comment éviter les transferts d'impacts de tels outils technologiques ? Comment s'assurer de la résilience de tels systèmes dépendants de matières et industries rares et importées ?

**Vers des villes résilientes**

C'est la dominante de nos politiques territoriales actuelles, avec des outils comme la ZAN, les subventions à l'ESS, les volontés de relocaliser la production et l'économie locale. La ville de Bordeaux en est un bon exemple.

**Vers des villes frugales**

La transition écologique et la lutte contre les changements climatiques concourent à un usage prudent des ressources épuisables et à la préservation des diversités biologiques et culturelles pour une planète meilleure à vivre. Le Manifeste pour une frugalité heureuse et créative imagine une urbanisation qui emploie avec soin le foncier et les ressources (locales), des constructions bioclimatiques, *low-tech* mais *high-knowledge* afin de répondre à nos normes de confort (qui sont d'ailleurs à réinterroger).

Se pose ici les questions d'acceptabilité et de généralisation de ces démarches dans notre contexte de croissance. Est-ce un récit que l'on peut intégrer ou bien est-ce une vision d'organisation post-crises ?

### 1.5 Evaluer autrement

Le contexte précédent montre deux choses :
- **L'activité humaine a pris une telle ampleur au cours des derniers siècles que les enjeux qui la caractérisent doivent être traité, et de manière urgente**, afin d'orienter la direction que prend notre société vers un monde souhaitable et agréable à vivre.
- Notre **capacité à faire évoluer le système se heurte à l'inertie de celui-ci, et à l'organisation sociale actuelle**.
- **La complexité du monde n'est pas comprise du tout**. Il faut multiplier les indicateurs et méthodologies, partout, pour enrayer la dynamique extractiviste-capitaliste actuelle et permettre à des nouveaux possibles d'émerger.

Il faut démocratiser les méthodes pour compter et évaluer autrement. Pour mesurer les "gains de qualité et de durabilité". Ce n'est pas une fin en soi, mais un moyen de contrer une vision comptable capitaliste afin de **déclencher les investissements nécessaires à la transition écologique et sociale**. Toutes les externalités positives et négatives doivent être prises en compte. Il faut revoir le concept de richesse : Le PIB ne prend pas en compte un grand nombre de bénéfices pour notre société (*le bénévolat, les activités domestiques, l'éducation des enfants, l'engagement associatif ou citoyen, les services environnementaux, le lien social et la proximité, etc.*).

## 2/ Présentation du projet

### 2.1 Génèse

Alors quels outils pour évaluer autrement les services écosystémiques rendus à la ville, le souci de la préservation des ressources (en matière première et d'énergie), d'écoconstruction sur la santé ? Quelles limites à cette démarche ? Comment ne pas tomber dans une vision court-termiste ? 

Beaucoup de travaux émergent ces dernières années, prenant place dans ce combat. Les méthodes pour évaluer les impacts actuels de l'activité humaine sur notre environnement sociotechnique se développent: bilan carbone, ACV environnementale... Cependant les travaux pour anticiper les impacts engendrés par les choix d'aujourd'hui sont plus rare. Et pourtant, c'est dès la phase de conception qu'il faut étudié les impacts avec un point de vue systémique et long terme.

<p align = "center" style="color:#556B2F"><b> Ce projet s'inscrit dans cette branche de l'évaluation d'impact: comment anticiper les effets et transferts d'impacts que les décisions d'aujourd'hui amèneront à demain </b></p>

La version Alpha du projet est issu du travail de recherche de Guilwen Meunier[^15], stagiaire chez Elémentaire conseil[^16]. Elémentaire conseil est un bureau d'étude énergétique et environnemental qui travail sur des projets d'aménagement principalement dans les phases de programmation et de conception: intervenir plus tard serait inutile car tous les choix auraient déjà été pris. Elementaire Conseil travaille au plus proche des filières locales (*Collectif paille Armoricain*[^17], ou *Collectif biosourcé*[^18]) afin d'amener les valeurs sociales et éthiques de ces milieux dans ses projets (en plus des enjeux liées à l'efficacité énergétique et à l'environnement). Avec leur recul et expérience, l'un des principales frein au passage d'échelle de ces pratiques est le manque d'arguments scientifiques, tenant tête à la rigueur économique d'un projet, pour convaincre des maitrises d'ouvrages frileuse de se lancer sur une voie plus vertueuse.

C'est de cette volonté qu'est né un premier stage sur le développement d'un outil d'analyse multicritère des effets et impacts d'un projet, afin d'aider à la décision les équipes de programmation avec lesquelles l'entreprise sera amené à travailler.

Ce projet s'inscrit ainsi **humblement** dans une volonté d'améliorer et de démocratiser les méthodologies d'analyse multicritère. C'est un projet en cours de développement et ouvert à tous retours que vous pourriez lui faire.


### 2.2 Définition du projet 

#### 2.2.1 Idéation

La première question fut sur la **pertinence d'une évaluation quantifiée pendant phase de programmation/conception ?**
En effet celle-ci demande d'y consacrer un temps relativement long, et surtout beaucoup de données.
Ce temps est à prendre. Puis, avec une méthodologie établit, un outil assez souple, et de l'open data, ce temps peut-être considérablement raccourci. Cependant, il faut bien commencer quelque part.

Il faut être conscient du caractère évolutif du projet, puisque à ce stade d'un projet, tout peut encore changer, et les données du projet sont par conséquent peu précise. Malgré cela, une évaluation quantifié pendant la phase de programmation permet une adhésion plus forte aux idées éthiques porté par un projet, à fournir des preuves scientifiques (argument d'autorité) pour débloquer des fonds de financement, ou bien favoriser une prise de "risque". C'est une arme recevable par le système conventionnel, qui ouvre le dialogue vers d'autres arguments que la facilité économique du "moins chère" et la subjective "beauté" apparente d'un projet. 


<p align="center">Quelle forme donner alors à un tel outil ?</p>

L'idée d'Elémentaire conseil est de proposer aux maitrise d'ouvrages (*et autres acteurs*) une image forte: celle du Donut de Kate Raworth. Avec en extérieur les impacts environnementales et en intérieur les impacts "humaine et sociale", par exemple:
- Social (isolation, collectivité, créateur de lien, prise en compte des transports)
- Impact sur les comportements (Greenwashing, empreinte fantôme, biais de culpabilité, fermeture )
- Résilience (Apprenant, Intégré, Robuste, Flexible, Autonome, Diversifié, Inclusif, Redondant )
- Dépendances (techniques, ressources, entretiens, humaines)
- Gouvernance

![Donut_nostro](Images/donut.png)

Cette idée est complété par trois volontés:
- **Apporter une vision systémique au projet**, dans l'optique d'étudier le métabolisme du territoire qui accueillera le projet, et ainsi comprendre le tissu social qu'il va influencer.
- **Ne pas compter en absolu, mais en relatif**: certaines données sociales ne sont pas quantifiables, les compter en absolu génère une subjectivité trop peu justifiable. **Les pistes théoriques de l'ACV sociale et du Design systémique** donne alors des éléments de réponse que l'outil souhaite s'approprier.
- **Inscrire le projet dans le domaine de la Science ouverte**, cela va conditionner de nombreux choix de développement et de diffusion du savoir créer par ce projet.

#### 2.2.2 Evaluation qualitative et réflexive à priori 

Cette étape devrait être automatique pour chaque projet. Elle a pour but d'évaluer la direction d'un projet dès l'idéation, avec quelques outils reconnus:
- Etude de la fermeture
- Etude de l'effet rebond
- Les 7 péchés du greenwashing
- Evaluation de l'empreinte fantôme du projet

(D'autres outils existent, comme la méthode "Diamond", la méthode du renoncement, l'estimation chiffrée, l'empreinte écologique et résiliente, les Domaines d'acitivés stratégiques).
Vous trouverez un très bon cours sur l'évaluation qualitative et réflexive à priori[^19] [ici](https://stph.librecours.net/modules/lownum/low03/web/co/low03.html).

Nous prenons le temps d'expliquer les quatre outils cité plus haut puisqu'ils servent à appréhender la complexité d'un projet. Ce sont en soit des outils de design systémique qui ont leur rôle à jouer dans ce projet d'outil.

##### a/ Etude de la fermeture

La fermeture se définit comme étant : l'organisation de la fin de la création, de la production et de l'usage de certaines technologies via la mise en place de stratégies de sobriété, voire de peurs ou d'interdits. J'ai donc évalué la fermeture de ce projet en me posant les deux questions fondamentales sur l'outil :
-   A: Quelles sont les infrastructures que notre projet pourrait conduire à fermer partiellement ou complètement ?
-   B: Quelles sont les politiques de fermeture qui pourraient favoriser la réalisation de notre projet ?

>A:
>L'outil a pour vocation de démocratiser l'évaluation multicritères, il n'y a pas ou peu d'outil qui y répondent. Le projet ne va pas fermer, même partiellement, mais contribuera plutôt au foisonnement de tels outils (de part sa propriété intellectuelle ouverte). Au contraire, comme pour les bases de données INIES, il y aura besoins d'expert pour uniformiser les différentes réalisations.
>B:
>Une politique pour favoriser la réalisation de ce projet serait la nécessité de fournir une étude systémique du projet à ses clients.

##### b/ Etude de l'effet rebond

Les effets rebonds regroupent les phénomènes d'optimisation d'un objet/service qui engendrent le contraire de ce qui avait été désiré c'est à dire une augmentation de la consommation de l'objet ou du service et donc une augmentation de son usage. Ces effets rebonds peuvent se traduire par une augmentation de la pression sur certaines matières premières, sur l'énergie…Dans une démarche d'amélioration les effets directs dont généralement positifs: réduction de l'empreinte environnementale sur les paramètres choisis. Effet rebond indirect: L'économie est réinvestie dans la consommation d'autres produits problématiques. Le service fournit permet parfois la réalisation d'évènements qui n'avaient pas été pensés lors que la réalisation du projet.

> Le risque d'effet rebond est présent. La réalisation de cette outil, s'il est mal utilisé pourrait donner raison à des projets qui ont un impact critique d'un point de vue politique ou juridique. Il est nécessaire d'intégrer le contexte du projet dans l'outil. Un autre effet rebond envisagé est celui d'une augmentation du nombre de projet si ceux-ci présentent une "bonne note" par leur coût global. Pour l'éviter, il ne faut considérer comme "bon" que ce qui est régénératif.

##### c/ Etude par les 7 péchés du Greenwashing

Le greenwashing est l'opposé d'une démarche honnête et rationnelle de mesure d'impact. Pour le détecter il est nécessaire de disposer d'une bonne connaissance de l'état de l'art des méthodes et études existantes pour ne pas tomber dans l'écueil.
Le greenwashing consiste à donner une image écologique à une marque, entreprise, un objet ou un service en mentant, cachant la vérité ou faisant des allégations non vérifiées à propos de bénéfices environnementaux. 7 procédés ont pu être mis au jour, et le but fut d'évaluer si nos propositions lors du maquettage sont pertinentes ou tombent sous le coup d'un péché.
Les 7 péchés:
1. Compromis caché: Prétention ne considérant qu'un nombre restreint d'attributs et en occultant le reste.
2. Absence de preuve: Prétention non étayée par une information facile à trouver et digne de confiance.
3. Imprécision: Prétention vague ou floue. 
4. Non pertinence: Prétention exacte mais inutile ou insignifiante. 
5. Moindre mal: Prétention exacte, mais sur une catégorie de produits globalement nocifs. 
6. Faux écolabel: Utilisation de labels internes à l'entreprise peu contraignants et non délivrés par un organisme tiers. 
7. Mensonge: Prétention fausse.

>Evaluation du projet vis à vis des péchés:
>1. l'approche systémique et multicritère est particulièrement soumise à ce péché. Il conviendra de faire preuve de beaucoup de transparence sur les choix et hypothèses effectués.
>2. L'objectif même de cette documentation est de fournir preuve et de rendre l'information disponible
>3. Introduction d'éléments de gestion de la précision dans l'outil
>4. Comme démontrer dans la partie précédente, la prétention n'est pas grande, mais la volonté l'est.
>5. Non concerné
>6. Non concerné
>7. L'argumentaire est toujours croisé avec plusieurs sources. 


--- 

##### d/ Notion d'empreinte fantôme du projet

L'empreinte fantôme se définit comme étant: l'ensemble des liens qu'une machine tisse avec le reste du monde.
Pour comprendre ces liens, on peut se poser les questions suivantes:
-   Quelle surface socio-environnementale une machine impacte-t-elle ?
-   Quelles sont les matières travaillées par la machine ?
-   Avec quelle facilité peut-elle être appropriée ?
-   Laisse-t-elle ou pas la place aux savoir-faire individuants ?
-   Est-elle compatible avec les rythmes culturels de ses opérateurs ?
-   Laisse-t-elle de la place pour une diversité de “chemins” disponibles ?
-   Qu'est ce que la machine empêche de se développer par ailleurs ?

On s'interroge alors sur:
- L'abstraction à la nature (le degré de dépassement des lois physiques grâce à la technique, et l'effet d'invisibilisation des cycles naturels que cela engendre.)
- Le Mythe de la transition (les systèmes techniques sont additifs, il ne font que s'empiler les uns sur les autres)
- La Prolétarisation (priver un sujet de ses savoir-faire, savoir vivre, savoir concevoir. Cela entraîne une dépossession du savoir-faire de l'artisan et un changement de rapport de force entre capital et travail: (l'ouvrier est substituable au contraire de l'artisan qui incorpore un savoir-faire)
- Cycle de vie et consommation (L'ACV rate le non quantitatif, le social (machines structure le rapport au monde, l'ACV intègre la mort des objets (Trier et recycler, et ne peut donc pas remettre en cause))).
- Dépendance à la technique (Dépendance au sentier: perte d'autonomie et autonomie technique)

L'empreinte fantôme prend alors la forme d'une carte mentale avec au centre le produit et en périphérie les éléments suivants :
-   Surface sociale impactée (activités humaines reconfigurées)
-   Surface environnementale impactée (milieux naturels reconfigurés)
-   Complexité (versus place aux savoir-faire individuants)
-   Contrainte d'usage (versus liberté d'usage et de non usage, rythmes culturellement choisis, multiplicité des chemins techniques)
Ensuite, attribuer des couleurs (vert à rouge par exemple) en fonction du degré de danger que l'on peut éviter en modifiant l'idéation.

Voici alors une **version simplifié de empreinte fantôme du projet à posteriori**, cet outil n'avais pas été réalisé en phase d'idéation (*par choix, sur le temps d'idéation prévu, nous avons choisit de réaliser les 7 péchés du greenwashing et l'étude de la fermeture et effet rebond*). Cette empreinte fantôme pourrait être largement compléter et approfondie, sa vocation est ainsi plus à montrer un exemple à dépasser qu'une référence.

![empreint_phantome](Images/1_empreinte.PNG)
<p align="center">Image 3: Empreinte fantôme du projet Donut: Version simplifié</p>

---


[^1]: Synthèse et analyse du nouveau rapport du GIEC [WWW Document], 2021. . Bon Pote. URL [https://bonpote.com/synthese-et-analyse-du-nouveau-rapport-du-giec/](https://bonpote.com/synthese-et-analyse-du-nouveau-rapport-du-giec/) (accessed 8.14.23).
[^2]:
[^3]:IPBES (2019): Summary for policymakers of the global assessment report on biodiversity and ecosystem services of the Intergovernmental Science-Policy Platform on Biodiversity and Ecosystem Services. S. Díaz, J. Settele, E. S. Brondízio E.S., H. T. Ngo, M. Guèze, J. Agard, A. Arneth, P. Balvanera, K. A. Brauman, S. H. M. Butchart, K. M. A. Chan, L. A. Garibaldi, K. Ichii, J. Liu, S. M. Subramanian, G. F. Midgley, P. Miloslavich, Z. Molnár, D. Obura, A. Pfaff, S. Polasky, A. Purvis, J. Razzaque, B. Reyers, R. Roy Chowdhury, Y. J. Shin, I. J. Visseren-Hamakers, K. J. Willis, and C. N. Zayas (eds.). IPBES secretariat, Bonn, Germany. 56 pages. https://www.ipbes.net/sites/default/files/2020-02/ipbes_global_assessment_report_summary_for_policymakers_fr.pdf (accessed 8.14.23)
[^4]: Le 7e continent de plastique : ces tourbillons de déchets dans les océans, 2012. Le Monde.fr.
[^5]: John Stuart Mill, Principes d'économie politique, 1844
[^6]: La rationalité de l’homo economicus : une hypothèse infondée - Fiche - The Other Economy [WWW Document], n.d. URL [https://theothereconomy.com/fr/fiches/la-rationalite-de-lhomo-economicus-une-hypothese-infondee/](https://theothereconomy.com/fr/fiches/la-rationalite-de-lhomo-economicus-une-hypothese-infondee/) (accessed 8.14.23).
[^7]:https://fr.wikipedia.org/wiki/Simon_Kuznets
[^8]: Anthropocene Economics and Design: Heterodox Economics for Design Transitions - Scientific Figure on ResearchGate. Available from: https://www.researchgate.net/figure/Circular-Flow-diagram-Copyright-C-2017-by-Paul-Samuelson_fig5_329877710 (accessed 8.14.2023)
[^9]: https://fr.wikipedia.org/wiki/Courbe_de_Kuznets
[^10]: Wilkinson, R. & Pickett, K. (2013). L’égalité, c’est mieux. Pourquoi les écarts de richesses ruinent nos sociétés. Montréal : Écosociété (375 pages). https://oraprdnt.uqtr.uquebec.ca/pls/public/docs/FWG/GSC/Publication/1935/46/3976/1/63502/10/F1952113253_35_2_233_238.pdf
[^11]: H. Simon "[Designing Organizations for an Information-Rich World](https://digitalcollections.library.cmu.edu/awweb/awarchive?type=file&item=33748) [[archive](https://archive.wikiwix.com/cache/?url=https%3A%2F%2Fdigitalcollections.library.cmu.edu%2Fawweb%2Fawarchive%3Ftype%3Dfile%26item%3D33748 "archive sur Wikiwix")]", in M. Grennberger, _Computer, communications and the public interest_. Baltimore MD : The Johns Hopkins Press, 1971, p. 37-72
[^12]: Dit", A.D.P. "Aussitôt, 2020. La crise écologique comme crise de la sensibilité/Baptiste Morizot/ Texte n°34. aussitôt dit. URL [https://aussitotdit.net/2020/08/17/la-crise-ecologique-comme-crise-de-la-sensibilite-baptiste-morizot-texte-n36/](https://aussitotdit.net/2020/08/17/la-crise-ecologique-comme-crise-de-la-sensibilite-baptiste-morizot-texte-n36/) (accessed 8.14.23).
[^13]: SIMAY, C., SIMAY, P., 2022. La Ferme Du Rail, Edition Domaine du possible. ed. Actes Sud.
[^14]:Bihouix, P., Jeantet, S., De Selva, C., 2022. La ville stationnaire: comment mettre fin à l’étalement urbain ?, Edition Domaine du possible. ed. Acte Sud.
[^15]: https://www.linkedin.com/in/guilwen-meunier/
[^16]: http://elementaire-conseil.fr/
[^17]: https://armorique.constructionpaille.fr/
[^18]: https://www.novabuild.fr/biosource
[^19]: Evaluation qualitative et réflexive a priori, Stephane Crozat, Audrey Guélou, UPLOAD-UTC, l'université populaire du libre ouverte accessible et décentralisée à l'UTC: https://stph.librecours.net/modules/lownum/low03/web/co/low03_1.html (accessed 8.14.2023)
