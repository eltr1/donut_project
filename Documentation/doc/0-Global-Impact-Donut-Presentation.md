# Global Impact Donut ?

## 1/ Le projet

| Projet | Version | dernière mise à jour | Auteur | Statut| License |
|:-:|:-:|:-:|:-:|:-:|:-:|
|Global Impact Donut (GIDo)| Alpha 1.1 | 18/08/2023 | Guilwen Meunier | En pause |CC-BY-SA|

Le projet Global Impact Donut (GIDo) s'inscrit ainsi **humblement** dans une volonté d'améliorer et de démocratiser les méthodologies d'analyse multicritère. C'est un projet en cours de développement, à l'initiative de Elémentaire Conseil en 2023. Ouvert à tous retours que vous pourriez lui faire. 
La version Alpha 1.1 est sortie le 18/08/2023. Une suite est prévu, mais pas de dates pour l'instant.

<p align = "center" style="color:#556B2F"><b> Comment anticiper les effets et transferts d'impacts que les décisions d'aujourd'hui amèneront à demain ? </b></p>

La version Alpha 1.1 de GIDo propose une structure pour évaluer en parallèle les aspects énergétiques, environnementaux, économique, ainsi que des enjeux sociaux et éthiques, dans des projets. Avec leur recul et expérience, Elémentaire Conseil trouve  que l'un des principales frein au passage d'échelle de ces pratiques est le manque d'arguments scientifiques, tenant tête à la rigueur économique d'un projet, pour convaincre des maitrises d'ouvrages frileuse de se lancer sur des voies plus vertueuses.

C'est de cette volonté qu'est né le Global Impact Donut (GIDo), au terme d'un stage de 4 mois réalisé par Guilwen Meunier. Le but est là: aide à la décision des équipes de programmation avec lesquelles l'entreprise sera amené à travailler.

L'idée d'Elémentaire conseil est de proposer aux maitrise d'ouvrages (*et autres acteurs*) une image forte: celle du Donut de Kate Raworth, afin de représenter les potentiels d'amélioration ou de dégradation d'un indicateur retenu, par exemple:
- Social (isolation, collectivité, créateur de lien, prise en compte des transports)
- Impact sur les comportements (Greenwashing, empreinte fantôme, biais de culpabilité, fermeture )
- Résilience (Apprenant, Intégré, Robuste, Flexible, Autonome, Diversifié, Inclusif, Redondant )
- Dépendances (techniques, ressources, entretiens, humaines)
- Gouvernance

![Donut_nostro](Images/donut.png)

Cette idée est complété par trois volontés:
- **Apporter une vision systémique au projet**, dans l'optique d'étudier le métabolisme du territoire qui accueillera le projet, et ainsi comprendre le tissu social qu'il va influencer.
- **Ne pas compter en absolu, mais en relatif**: certaines données sociales ne sont pas quantifiables, les compter en absolu génère une subjectivité trop peu justifiable. **Les pistes théoriques de l'ACV sociale et du Design systémique** donne alors des éléments de réponse que l'outil souhaite s'approprier.
- **Inscrire le projet dans le domaine de la Science ouverte**, cela va conditionner de nombreux choix de développement et de diffusion du savoir créer par ce projet.
## 2/ Vue d'ensemble

Le projet est constitué de:
- Dépot GitLab du code et de la documentation.
- Wiki hébergé par Readthedoc.

![Schéma vue d'ensemble](Images/schema_vue_global.png)
<p align="center">Schéma vue d'ensemble du projet</p>
