Graph
=====
.. automodule:: Graph
.. autoclass:: Graph.Parameters
    :members:

.. autofunction:: Graph.plot_donut
.. autofunction:: Graph.plot_type_1
.. autofunction:: Graph.plot_type_2