Weighting
=========
.. automodule:: Assessment
.. autofunction:: Assessment.get_pathway_list
.. autofunction:: Assessment.get_comparison