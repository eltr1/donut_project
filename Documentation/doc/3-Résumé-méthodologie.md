# 3 Résumé méthodologie

![image](Images/schema_Algo_Alpha_vertical.png)

## Partie 1: Goal & Scope

L'étape *Objectif & Périmètre* a plusieurs but:
- Amener les parties prenantes à une meilleure compréhension de leur projet, en adoptant un point de vue systémique et multicritère.
- Déterminer les périmètres contexto-spatio-temporel du projet.
- Préparer et orienter la récolte d'informations vers le plus de pertinence et de justesse possible.
- Clarifier l'éthique du projet et donc clarifier le modèle de valeur du projet.

Cette étape demande à l'équipe de travailler en appliquant des outils de design systémique afin d'appréhender la complexité du système, puis d'établir les périmètres, et enfin de délimiter le périmètre d'impact.

## Partie 2: LCI

Le Life Cycle Inventory (LCI) consiste en la collecte des variables d'entrées de tous les pathways activés. La collecte s'attarde sur:
- Les données de localisation, de temporalité et de contexte, aux différentes échelles du projet. 
- Le niveau de précision de ces données, selon 4 dimensions (temporelle, géographique, résilience, précision).
Une fois ces informations collectées, elles sont consignées dans les bases de données de type LCI du tableur du projet. 

## Partie 3: LCIA

Le Life Cycle Inventory Assessment (LCIA) est l'étape d'évaluation et de normalisation des pathways. 
### Evaluation

Lors de l'évaluation des pathways, la diversité des natures des informations impose plusieurs cas:
- La méthode calcul() transforme les variables d'entrées en impact potentiel absolu non représentatif, selon une relation causale définit mathématiquement.
- La méthode estimate() transforme des notations relatives d'impacts potentiels en impact potentiel.

### Normalisation

L'étape de normalisation va transformer les valeurs précédentes en impact réel effective sur une capabilité du modèle de valeur. Ces impacts sont positives et non nulles. Elles représentent l'appréciation (Impact > 100%) ou la dépréciation (Impact < 100%) d'une capabilité par rapport à une situation de  référence.
## Partie 4: Analysis & results

### Agrégation

Le choix du modèle de valeur permet de déterminer quels aires de protection (AoP) le projet souhaite s'intéressé, et détermine donc les catégories d'Endpoints qui constitueront le Donut. Le modèle de valeur permet d'agréger les impacts réels effectif sur les capabilités selon une somme pondérée par la criticité établit pour chaque Endpoint.

### 4.2 Analyse des résultats

L'interface graphique n'est pas à délaisser. La mise en forme est primordiale. Les images ont une influence forte dans la compréhension de système complexe. Ces représentations sont nécessaires pour une prise de décision éclairée. 
- Le menu permet d'interagir avec les paramètres de la méthode pour effectuer de nouvelles comparaisons d'impacts.
- L'affichage du Donut donne une visualisation puissante pour comprendre les impacts d'un système.
