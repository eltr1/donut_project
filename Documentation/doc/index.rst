.. Projet Donut documentation master file, created by
   sphinx-quickstart on Wed Jul  5 11:57:26 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Projet Donut's documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   0-Global-Impact-Donut-Presentation.md
   1-Génèse-et-concepts.md
   2-Guide.md
   3-Résumé-méthodologie.md
   4-Objectifs-périmètre.md
   5-LCI.md
   6-Evaluation-pathway.md
   7-Modèle-de-valeur.md
   8-Interface-Graphique.md
   9-Lexique.md
   Bibliographie.md

   code/main.rst
   code/Graph.rst
   code/Assessment.rst
   code/Pathways.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
