# 7/ Modèle de valeur
## Résumé:

Le choix du modèle de valeur permet de déterminer quels aires de protection (AoP) le projet souhaite s'intéressé, et détermine donc les catégories d'Endpoints qui constitueront le Donut. Le modèle de valeur permet d'agréger les impacts réels effectif sur les capabilités selon une somme pondérée par la criticité établit pour chaque Endpoint.

---

## 1/ Objectif de la démarche d'agrégation

### 1.1 Pourquoi agréger ?

Les résultats d'une étude multicritère sont loin d'être facilement assimilable et appropriable. Cela tient de l'étendue spatio-temporel des résultats ainsi que de la diversité de la nature des impacts. C'est des problèmes majeurs observé en ACV:  la diversité des unités des indicateurs rend la restitution des résultats très difficile. C'est pourquoi très souvent on se limite au poids seul de l'indicateur carbone, pour donner l'image d'une prise en compte de l'environnement avant de prendre des décisions financières. C'est un début nécessaire, impulsé par les normes et législation, mais qui ne reflète pas une prise en compte forte et systémique de l'environnement.
C'est particulièrement vrai dans le contexte du bâtiment, cependant l'envie de mettre en avant d'autres indicateurs pour alimenter les réflexions plus loin que le carbone se fait sentir de partout, une volonté d'approche plus globale, plus systémique se fait sentir par de nombreux labels (HQE, Bâtiment francilien, Biodivercity...) ainsi que de nombreux accompagnements vers la résilience etc...

L'étape d'agrégation fera certes perdre de l'information, mais permet une appropriation des résultats plus forte, et découle une implication plus grande des parties prenantes. La drôle d'équation de J.L. Swiners et J.M. Briet [^1] nous l'exprime bien: 

$E=Q * A$

Autrement dit, l'efficacité d'une mesure est égale à sa qualité multipliée par l'adhésion qu'elle remporte. On peut comprendre que bien qu'une étude multicritère complexe soit réalisé, si les résultats ne provoque pas de l'adhésion, les résultats que cette étude entend n'auront que peu d'impact sur le projet.

### 1.2 Quelques lectures inspirantes 

Les praticiens d'ACV se basent sur des Aires de protections (AoP) pour évaluer les impacts structurants pour la société. En ACV environnementale, les AoP sont santé humaine, ressources naturelles, environnements naturels et fabriqués par l'homme.
Dès lors que l'on souhaite agréger des impacts sociaux, comme en ACV sociale, il n'y a pas de consensus théorique. Alors, comment sont transformés et interprétés les résultats d'analyses multicritères ? 
Dans l'étude de l'existant, nous retiendrons plusieurs approches intéressantes:

#### 0.1.1Ecopoint

L'Ecopoint est une méthode d'agrégation proposée dans The Green Guide To Specification [^2], ouvrage réalisé par BRE (“The Green Guide Explained : BRE Group,” n.d.) .
C'est le plus simple et facilement applicable: on vient normaliser et pondérer en fonction des données d'impact d'un habitant européen moyen.  On a alors:

$Ecopoint = \sum_{i=1}^{n} ( Impact * Weight)$

C'est un exemple de généralisation que l'on souhaite éviter puisque l'on perd la quasi-totalité de l'information lié au projet, son contexte... et que la valeur obtenu ne reflète aucune réalité à laquelle se rattacher pour l'interpréter.

#### 0.1.2 Impact 2002+

Une deuxième méthode est celle d'impact 2002+ [^3] (Jolliet et al., 2003) , qui vient catégoriser les indicateurs afin de les  pondérer par "paquet", avec des liens plus clair et identifiable. Cependant cette méthode est basée sur une ancienne norme (et n'est donc plus à jour): ses indicateurs d'impacts sont limités aux résultats d'une ACV environnementale.

![Image](https://www.researchgate.net/profile/A-Moign/publication/286452512/figure/fig4/AS:305393881370626@1449822983523/Indicateurs-et-categories-dimpact-de-la-methode-impact-2002.png)
<p align="center"><i> Image 1: Catégories de dommages de la méthode IMPACT 2002+ </i> </p>

#### 0.1.3 Vers un impact sur le Co2/hab

Une troisième méthode remarquable est la proposition du cabinet Elioth (Urvoy J., Gontier L, MEUNIER G, 2022) qui normalise en passant par l'empreinte carbone des habitants[^4]. Ainsi Elioth a développé 3 modèles (population, budget et mobility) qui l'aide à diagnostiquer le quartier dans lequel sera implanter le projet urbain. Enfin un modèle input/ouput a été développé pour modéliser la consommation de bien et de service. En se basant sur Eurostat et des ressources européennes, Elioth a créé une base de convertisseurs pour les 64 flux économiques principaux. Cette approche est particulièrement intéressante puisqu'elle possède des modèles statistiques pour identifier des leviers potentiels et sa capacité à ramener les impacts à l'empreinte carbone individuelle permet une visualisation simple. Cependant sa finalité carbone reste monocritère et ne prend pas en compte des finalités sociales, et de résilience.

Les principaux exemples précédents portent sur une agrégation de ceux que l'on nomme des midpoints en ACV sociale. La méthodologie étant d'emblée différente (avec potentiellement plusieurs AoP), il semble peu pertinent d'agréger les impacts sur AoP obtenus pour chacune des parties impactées. On retiendra cependant un essai d'agrégation (Macombe et al., 2013) en comptabilisant le nombre d'impact faiblement positif, très positif et inversement faiblement et très négatif. On peut alors exprimer le résultat par phrase ; " ce projet est à 74% positif" et comparer globalement plusieurs scénarios. 
Cependant cela ne prend pas en compte directement les importances relatives de chaque indicateur. Faut-il les intégrer dans la démarche d'agrégation des midpoints ?


### 1.3 Besoin d'un modèle éthique

Maintenant, avant d'agréger, il convient de se demander comment se font les choix d'agrégations : **quel est la finalité que l'outil représente ?** Rappelons que l'objectif de la méthodologie est de **préserver une zone de protection (AoP).**

En ACV-S, l'espérance de vie à la naissance (LEX) est une AoP couramment utilisée. L'avantage de LEX est sa simplicité d'usage (*c.a.d. facilement quantifiable*) et son auto-évidence (*provenant d'une acceptation utilitariste simpliste*). Nous avons déjà vu avec le choix des capabilités en quoi cette éthique posait problème à la méthodologie. Plus généralement, on peut se passer d'éthique lorsque l'on mesure des données quantifiables, c'est le cas en ACV environnementale. Cependant, dès lors que l'on intègre des données uniquement qualifiables, dépendant du contexte, de la culture... alors il faut exprimer et assumer le modèle de valeur utilisés afin d'assurer une légitimité à la cohérence proposée.

Il convient alors d'explorer les Endpoints et différents autres indicateurs avec un nouveau regard, puis de proposer un schéma de construction de modèle de valeurs adapté à notre outil: Il convient de réintégrer la question éthique au cœur de la méthodologie.

## 2/ Modèle de valeurs

### 2.1 Caractéristiques du modèle de valeurs

Le modèle de valeurs sert de représentation pour les principes, l'éthique, la responsabilité et l'ancrage que l'on souhaite donner au projet. Ces modèles sont plus ou moins subjectifs et complet. Le modèle de valeurs traduit une ou des aires de protections (AoP) qui sont intrinsèquement liées à une amélioration de la qualité de vie d'un groupe social identifié. Ainsi, le modèle de valeurs vient servir de support à l'agrégation des indicateurs Endpoints issus de l'évaluation des pathways.

McGillivray (2007) présente la conception de Finnis (1980) selon laquelle les dimensions du bien-être sont : 
- (1) **Auto-évidentes**, en ce sens qu'elles sont potentiellement reconnaissables par n'importe qui ; 
- (2) **Incommensurables**, dans le sens où toutes les qualités souhaitables de l'une ne sont pas présentes dans l'autre ; 
- (3) **Irréductibles**, car il n'existe pas de dénominateur unique auquel elles peuvent être totalement réduites ;  
- (4) **non hiérarchiques**, puisqu'à tout moment une dimension peut sembler la plus importante (Alkire 2002).

Ces caractérisations ne préconisent donc pas une agrégation unique, malgré l'avantage compréhensif que cela peut générer, et que la hiérarchisation des dimensions composants le modèle de valeurs ne sont pas fixées mais évolutives.

Il convient de plus dans le choix de ce modèle, de respecter les hypothèses de notre travail: 
- Nous cherchons à représenter un modèle chaotique dynamique, il convient alors de vérifier l'axiome de relativité, subjectivité et de diversité, de transfert d'impact.
- Ce modèle représente aussi une projection des valeurs portées par les partis prenantes dans l'évaluation des impacts potentiels: il faut que le choix du modèle soit fait par ces partis prenantes, et que ce choix les embarque avec lui.
- Par cohérence, nous souhaitons différencier les effets d'impacts sur chacun des partis impactés. Cela implique que le modèle de valeur est déclinable pour chaque parti impacté.

### 2.2 Quelques exemples de modèle de valeurs

Il existe plusieurs schémas de modèle des valeurs, plus ou moins aboutit, référencé ici et qui peuvent servir d'illustration à l'élaboration d'un modèle de valeur pour l'outil.

#### 2.2.1 ODD et sub-catégories

L'agenda 2030 est un programme universel pour le développement durable. Il porte l'ambition de transformer notre monde en éradiquant la pauvreté et les inégalités en assurant sa transition écologique et solidaire à l'horizon 2030. Il présente 17 objectifs de développements:

![ODD](Images/ODD.png)
<p align="center"><i> Image 2: Les 17 Objectifs de Développement Durable. </i> </p>

Trois buts essentiels sont poursuivis : lutter contre les inégalités, l'exclusion et les injustices ; faire face au défi climatique ; mettre fin à l'extrême pauvreté.

Le cadre d'indicateurs mondial a ensuite été adopté par l'Assemblée générale le 6 juillet 2017 et figure dans la résolution adoptée par l'Assemblée générale sur les travaux de la Commission de statistique relatifs au Programme de développement durable à l'horizon 2030. Selon la résolution, le cadre d'indicateurs sera affiné chaque année. Il sera aussi examiné de manière exhaustive par la Commission de statistique en mars 2020 puis en 2025. Le cadre d'indicateurs global sera complété par des indicateurs aux niveaux régional et national, qui seront développés par les États membres.

Le cadre global d'indicateurs comprend 231 indicateurs uniques [^5].  Les différents états sont ensuite priées de créer un modèle qui leur correspond.

À l'issue d'une concertation menée sous l'égide du Conseil national de l’Information statistique (Cnis) a été proposé mi-2018 un tableau de bord de 98 indicateurs [^6] qui constituent le cadre national pour le suivi des progrès de la France dans l’atteinte des 17 ODD. 

#### 2.2.2 Subcategories for S-LCA

Les fiches méthodologiques pour les sous-catégories de l'ACV-S complètent les lignes directrices pour l'analyse du cycle de vie social des produits et des organisations publiées par l'initiative du PNUE sur le cycle de vie.
Les produits et organisations publiées par l'initiative du PNUE sur le cycle de vie et ont été élaborées en tant que ressource publique pour guider l'application de l'ACV-S.
L'objectif poursuivi par ces fiches est d'être un outil opérationnel pour les experts et les non-experts qui souhaitent concevoir et réaliser des ACV-S. Ces fiches ont été élaborées dans le cadre de l'Initiative pour le cycle de vie du PNUE.

![SLCA-subcategories](Images/SLCA-subcategories.PNG)
<p align="center"><i> Tableau 1: Stakeholder categories and subcategories in the UNEP 2020 Guidelines. </i> </p>

Les fiches ont été élaborées en tenant compte du fait que la collecte de données est l'activité qui demande le plus de travail lors de la réalisation d'une ACV sociale. Par conséquent, différents indicateurs peuvent être utilisés en fonction de la disponibilité des données et de l'objectif et de la portée de l'étude. Les fiches ont pour but d'inspirer des études de cas d'ACV-S basées sur les lignes directrices et non de représenter un ensemble complet d'indicateurs à inclure et à utiliser.

#### 2.2.3 Les indicateurs de Résilience 

"*Depuis 2020, le Cerema développe une méthodologie pour accompagner les réflexions autour de la résilience, qui s'appuie notamment sur la boussole de la résilience et fournit un cadre d'action destiné à renforcer l'adaptation des territoires aux différents événements qui peuvent survenir.*"

Comment les territoires, mais aussi les organisations, peuvent-ils se préparer, s’adapter, se transformer, pour mieux faire face aux perturbations ponctuelles et aux évolutions de long terme comme le changement climatique ou l’effondrement de la biodiversité ? L’approche globale et transversale de l'adaptation que porte le Cerema dessine des réponses à ces questions et des pistes d’action à saisir.

![SLCA-subcategories](Images/boussole_complete_v2.png)
<p align="center"><i> Image 3: Boussole de la résilience, CEREMA </i> </p>

Cette boussole est assez orientée vers l'adaptation aux crises sociales-environnementales actuelles, elle met en lumière les 6 piliers de la résilience ainsi que 18 indicateurs répartis dans ces piliers.

#### 2.2.4 Capabilités et indicateurs de bien-être

L'idée des capabilités vient de la volonté de Sen et Nussbaum de caractériser l'accès aux besoins fondamentaux comme des capacités de fonctionnement nécessaires. Avant eux, dans le domaine de la psychologie, Maslow (1954) et, plus récemment, Max-Neef (1992) ont proposé des ensembles de besoins humains fondamentaux. Deci (1995) et ses collègues ont identifié trois besoins psychologiques fondamentaux qui sont mis en évidence de manière empirique à travers les cultures, l'âge, le sexe et le statut économique : ceux qu'ils appellent l'autonomie, la compétence et la relation. Les besoins humains ne sont pas considérés comme un bien-être en soi, mais plutôt comme des conditions préalables au bien-être (Alkire, 2002).

Nussbaum (1998) propose un ensemble de capacités de fonctionnement de base qui sont censées être universellement applicables dans toutes les cultures. Ces capabilités de base répondent pour Nussbaum au minimum exigé par la dignité humaine:
-  la vie
- la santé du corps
- l’intégrité du corps
- les sens (associés à l’imagination et la pensée)
- les émotions
- la raison pratique
- l’affiliation
- les autres espèces
- le jeu
- le contrôle sur son environnement. 

Le pouvoir de cette théorie des capabilités et son potentiel pour renverser nos relations aux vivants, à la nature, aux autres. Cependant on peut lui faire une critique fondée sur sa naïveté d'application: les obstacles politiques à la mise en place d'un modèle basé sur la dignité humaine ne sont pas représentés. Cette vision d'indicateurs tend ainsi vers un usage éthique philosophique qu'un usage politique.



### 2.3 Elaboration d'un modèle de valeur

La version alpha ne possède pas de modèle de valeurs finalisés. L'outil est en soit utilisable pour ceux qui prendront le temps de faire un modèle de valeur. Des stages supplémentaires permettrons d'aboutir à un modèle de valeur intéressant.

La version alpha s'est contenté de comparer plusieurs modèles de valeurs existants, afin de croiser les modèles. Le modèle obtenu ressemble à cela:

![modèle_valeur](Images/modele_valeur.PNG)

Ce modèle est loin d'être fini. Cependant, nous pouvons déjà commenté la direction prise. Nous considérons 3 aires de protections, issues du modèle RAH développé par Symbiosis in Developpment. Ces AoP sont donc:

- Résilience
![résilience_model](Images/modele_resilience.PNG)

- Autonomie
![autonomy_model](Images/modele_autonomy.PNG)

- Harmonie
![harmony_model](Images/modele_harmony.PNG)

Dans chacun des AoP, nous avons commencé à exprimer des Endpoints numérotées. Ceux-ci sont issus des travaux du PNUE sur les catégories d'impacts en ACV sociale, des 17 ODD, des indicateurs de résiliences de différents groupes (CEREMA notamment), ainsi que de mes propres idées sur les impacts liés aux bâtiments et à l'information.

Les étapes suivantes à poursuivre seraient:
- Compléter la mindmap.
- Pour chaque nœud, renseigner les catégories de partie prenante impactée concernées
- Pour chaque nœud, lui attribuer un poids en fonction de son importance et de sa criticité dans le projet. La somme des poids d'un ensemble de frère doit être 1.

Afin de réaliser ce modèle de valeur, nous avons essayé plusieurs softwares. Le plus proche de nos attentes est Minpmap. Cependant, l'attribution de poids et d'attributs pour chaque nœuds n'est pas optimisé pour nos usages. Il convient de chercher un software plus proche de nos besoins.

## 3/ Application Donut: méthodologie d'agrégation

### 3.1 Fonctionnement général

Pendant le Life Cycle Inventory Assessment (*LCIA*) d'un scenario, **toutes les données sont stockées dans une liste**. Cette liste contient des éléments de type Class Object CreatePathway (*voir sa documentation dans Pathways*). Cela signifie que pour chaque élément de la liste -- donc chaque *pathway* -- nous allons appeler ses méthodes propres pour récupérer les paramètres d'entrée et calculer la valeur de la sortie. Toutes les données d'entrée et de sortie sont **stockées dans les attributs de classe de leur *pathway***. Autrement dit, nous avons une liste d'objet qui contiennent en eux tous leurs paramètres (données d'entrée, de sortie, précision, méthode de calcul, facteur de réalisation ou typologie de courbe).

Dans son fonctionnement, expliqué en partie 6, l'algorithme va alors pouvoir venir piocher dans les attributs de chaque élément de la liste ce dont il a besoin pour calculer, normaliser, pondérer... et en retour stocker la nouvelle valeur résultante dans l'attribut **self.value_n** de cet élément.

L'étape suivante consiste à agréger ces impacts réels par paquets d'impact sur un Endpoint. Les différents Endpoints dépendent du modèle de valeurs et sont souvent différents entre chaque catégorie de partie prenante. 


### 3.2 Mise en place dans l'outil

#### Modèle de valeur et base de données

Le modèle de valeurs conditionne les indicateurs/Endpoints que l'outil va utiliser. La base de données des pathways est prévu pour posséder un attribut d'activation[^7]. Ainsi il est possible, en fonction des valeurs du modèle de valeurs, d'utiliser ou nom un chemin. Cette souplesse permet notamment d'implémenter différents modèles de valeurs dans l'outils, ou de faire évoluer les priorités entre les différentes dimensions d'un même modèle.

Dans la version alpha, cette activation est manuelle. Si l'on ajoute ou enlève un Endpoint, il convient de désactiver les pathways concernés.

#### Modèle de valeur et agrégation

Dans l'outil, le modèle da valeurs est traduit par la base de données des Endpoints. Si jamais la traduction d'un certains Endpoint est trop complexe, il convient d'intégrer les midpoints dans la définition d'un chemin plus gros. Ce jonglage entre chemins et Endpoint permet de représenter le modèle de valeurs complétement.

Voir la documentation de la base de données Endpoint pour plus de détails.


[^2]: The Green Guide Explained : BRE Group [WWW Document], n.d. URL [https://tools.bregroup.com/greenguide/page.jsp?id=3612](https://tools.bregroup.com/greenguide/page.jsp?id=3612) (accessed 6.29.23).
[^3]: Jolliet, O., Margni, M., Charles, R., Humbert, S., Payet, J., Rebitzer, G., Rosenbaum, R., 2003. IMPACT 2002+: A new life cycle impact assessment methodology. Int J LCA 8, 324. [https://doi.org/10.1007/BF02978505](https://doi.org/10.1007/BF02978505)
[^4]: Urvoy J., Gontier L, MEUNIER G, 2022. Du mètre carré à l’usager : Passer de l’ACV du bâtiment à l’empreinte carbone de ses habitants - Elioth [WWW Document]. Elioth by Egis. URL [https://elioth.com/du-metre-carre-a-lusager-passer-de-lacv-du-batiment-a-lempreinte-carbone-de-ses-habitants/](https://elioth.com/du-metre-carre-a-lusager-passer-de-lacv-du-batiment-a-lempreinte-carbone-de-ses-habitants/) (accessed 6.29.23).
[^5]: SDG Indicators — SDG Indicators [WWW Document], n.d. URL [https://unstats.un.org/sdgs/indicators/indicators-list/](https://unstats.un.org/sdgs/indicators/indicators-list/) (accessed 8.7.23).
[^6]: L'indicateurs pour le suivi national des objectifs de développement durable | Insee [WWW Document], n.d. URL [https://www.insee.fr/fr/statistiques/2654964](https://www.insee.fr/fr/statistiques/2654964) (accessed 8.7.23).
[^7]: Voir LCI, 3/ Structure des bases de données