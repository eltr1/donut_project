The research project will end in September 2023.

The tool will be available in its Alpha.1 version.

For the time being, I don't plan to come back to work on it, so feel free to improve it yourself and share the tool.
Have a look at the contributors' guide, where you'll find detailed instructions on how to get to grips with the tool's code and methodology.

If you decide to implement new features, please do so under the CC-BY-SA license.
Don't hesitate to send me a message when you've improved the tool, I'm already curious to see what it would have become!


This work was carried out by Guilwen Meunier, during my 2nd year internship (STING) at Centrale Nantes engineering school.
This work was made possible by Nicolas Naud of [Elementaire Conseil] (http://elementaire-conseil.fr/).

If you ever refers to this work, please use the following:
> Meunier G, Naud N, 2023. Global Impact Donut - GIDo. URL https://gitlab.com/eltr1/donut_project
