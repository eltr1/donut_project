#!/bin/sh

echo "Let's build your Donut Tool Environnement !"
echo "[10%] checking the virtual environment..."
if [ ! -d "$venv" ];
then
  echo "[20%] No virtual environment found... Building a brand new one !"
  py -m venv venv
fi

cd venv/Scripts
touch activate.bat
cd ..
cd ..
echo "[50%] Virtual Environnement is operable..."
echo "[60%] Checking for updates..."

py -m ensurepip --

echo "[70%] Installing libraries and dependencies..."

py -m pip install -r requirement.txt

echo "[100%] Dependencies Installed !"
echo "[100%] Yay"
echo "[100%] Closing down the Installation process and loading the Donut tool"

echo "---"
echo "Welcome to the Donut tool"
echo "---"
echo "RUN: Just run 'py main.py' command to start the Donut tool"
echo "HELP: go to the wiki for details (link in the Gitlab project README)"


$SHELL
