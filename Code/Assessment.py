"""
Assessment.py contains:

- get_pathway_list(): correspond to the LCIA Evaluate pathway stage.
- get_comparison(): correspond to the LCIA Normalisation and agregation stage.

Be careful: those function are coded to use a specific DataFrame structure.
Please refer to the guide if you have a problem.
"""

from math import sqrt
from Code.Pathways import *

##################################################################################################
def get_pathway_list(pathways_data, LCI_table):
    """
    This function parameters are the DataFrame of both the pathways and scenario pieces of information.
    It returns a list of class object pathways, each pathway has their ID, value, precision... in their self parameters.
    Refer to Pathways.py for further understanding of the CreatePathway class.

    :param panda.DataFrame pathways_data: The pathway_data sheet, a pandas database that recap every pathways
        specificities, containing input and output information.

    :param panda.DataFrame LCI_table: A panda database of the LCI of a scenario or reference.
    """
    pathways_list = []
    for index in pathways_data.index:

        # We only want to use pathway that are needed for the simulation:
        if pathways_data.at[index, 'Activate'] == 1:
            ######################################################################
            # Let's create a class pathway object:
            id_pathway = int(pathways_data.at[index, 'Pathway_ID'])
            pathway = CreatePathway(id_pathway)

            # Function to take values from datasheet:
            pathway.get_info(pathways_data)
            pathway.get_precision(LCI_table)

            ######################################################################
            # Calcul of the pathway output, stored in self.value :
            if pathway.type == 1:
                pathway.calcul(LCI_table)

            if pathway.type == 2:
                pathway.estimate_homogeneisation(LCI_table)
            # We add the class object pathway to our list of pathways.
            pathways_list.append(pathway)

    return pathways_list


#################################################################################################
def get_comparison(database_endpoint_impact, pathways_list, name_scenario, pathways_list_ref, error_shown):
    """
    The get_comparison function will determine the values to use in the donut graph.

    :param panda.DataFrame database_endpoint_impact:
        The database Graph_recap, containing the capability impact category to beshown on the graph.

    :param list[CreatePathway] pathways_list: A list containing CreatePathway class object, representing each
        pathway for the scenarion. With self.value as the pathway output, self.stakeholder the group output and
        self.precision as the pathway precision.

    :param str name_scenario: A python string containing the name of the scenario.

    :param list[CreatePathway] pathways_list_ref:  A list containing CreatePathway class object,
        representing each pathway for the reference. With self.value as the pathway output,
        self.stakeholder the group output and self.precision asthe pathway precision.

    :param int error_shown: An integer between [0,3]. It indicate which precision dimension use for the errorbars.
        Currently, its value is forced on 0. We hope to add a button to choose its value in a next update.
    """
    ######################################################################
    # Homogénéisation & Normalisation
    # Loop for each pathway in a scenario
    for n_pathway in range(0, len(pathways_list)):
        pathway = pathways_list[n_pathway]
        pathway_ref = pathways_list_ref[n_pathway]

        # Type 1 pathway are not yet relative and normalized
        if pathway.type == 1:
            # Normalisation by the reference value for the pathway
            pathway.value_n = pathway.value / pathway_ref.value

            # Transforming in a effective impact on capabilities
            pathway.value_n = pathway.value_n * pathway.realisation

        # Type 2 pathway statistical method to normalize them
        if pathway.type == 2:
            pathway.estimate_statistical(pathway_ref)

        # Gestion des incertitudes
        nb_precision = len(pathway.precision)
        DeltaP = []
        x, y = pathway.precision, pathway_ref.precision
        vX, vY = pathway.value, pathway_ref.value

        for i in range(0, nb_precision):
            # Moyenne des précisions des entrées par type
            DeltaP.append(pathway.realisation * abs(pathway.value_n))

            # Transformation en incertitude (Subjectivement) et calcul de l'incertitude de f(X) = X/Y
            Dx, Dy = (1 - (x[i] / 10 + 0.5)) * vX, (1 - (y[i] / 10 + 0.5)) * vY
            if vX == 0:
                if vY != 0:
                    DeltaP[i] = pathway.realisation * abs(pathway.value_n) * Dy / vY
            if vY == 0:
                if vX != 0:
                    DeltaP[i] = pathway.realisation * abs(pathway.value_n) * Dx / vX
            if vX != 0 and vY != 0:
                DeltaP[i] = pathway.realisation * abs(pathway.value_n) * sqrt((Dx / vX)**2 + (Dy / vY)**2)

        # On a 4 precisions pour chacune des catégories de précision, on affiche celle de error_shown.
        pathway.precision = DeltaP[error_shown]

    ######################################################################
    # Ponderation & Aggregation
    # for loop to calculate the data for each impact category
    for index in database_endpoint_impact.index:

        # a/ Loading data
        # Recovering of the pathways which have an output related to the category
        pathways_id = str(database_endpoint_impact.loc[database_endpoint_impact.index == index, 'Pathways'].iloc[0])
        pathways_id = pathways_id.replace(' ', '').replace('[', '').replace(']', '').split(';')
        pathways_id = list(map(int, pathways_id))

        # Recovering of the weight of those pathways
        weight = str(database_endpoint_impact.loc[database_endpoint_impact.index == index, 'Weights'].iloc[0])
        weight = weight.replace(' ', '').replace('[', '').replace(']', '').replace(',', '.').split(';')
        weight = list(map(float, weight))

        #######################################################################
        # b/ Core graph database creation
        objective = str(database_endpoint_impact.loc[database_endpoint_impact.index == index, 'Objective'].iloc[0])
        if objective == 'nan':
            objective = 1

        # Loop for each entry
        number = -1
        scenario_ponderate = 0
        DeltaP = 0
        for entry_id in pathways_id:
            number += 1
            for pathway in pathways_list:
                if entry_id == pathway.ID:
                    scenario_ponderate += pathway.value_n * weight[number]
                    DeltaP += (pathway.precision * weight[number])
            # Store the value of the capability impact for each scenario
            database_endpoint_impact.loc[index, name_scenario] = scenario_ponderate / float(objective)
            database_endpoint_impact.loc[index, 'Errorbar'] = float(DeltaP)

    return database_endpoint_impact


###########################################################################################################
