"""
This python file contains the code to plot a graph of the environnemental and social impacts:
- class Parameters
- plot_donut()
- plot_type_1()
- plot_type_2()
"""

import numpy as np
from textwrap import wrap
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, CheckButtons

######################################################################################################################
class Parameters:
    """
    This class contains the graphic's parameters (Tkinter window, defined in main.py).
    It's attributes are:

    - self.mode :
        (integer) "1" if specific stakeholder is checked, used to plot with plot_type_1().
        "2" if multiple stakeholder is checked, used to plot with plot_type_2().
    - self.ref_name :
        (string) The name of the chosen reference.
    - self.stakeholder :
        (string) The name of the chosen stakeholder.
    - self.scenario_name :
        (string) The name of the chosen scenario.
    - self.List_name :
        (list of strings) Contains the name of each scenarios.
    - self.error_shown :
        (integer) parameter to decide which precision's dimension errorbar use in the plotting.
        By default, it is on '0', for Reliability of the source.
    - self.output :
        (pd.DataFrame) a panda Database containing database_endpoint_impact with two news columns:
        real impact on endpoints capabilities and their precision's dimension value (dimension controlled by errorshown)
    """
    def __init__(self):
        self.mode = 1
        self.ref_name = 0
        self.stakeholder = 'Local_Community'
        self.scenario_name = 0
        self.List_name = ['']
        self.error_shown = 0
        self.output = None



######################################################################################################################
# Here is graph_test function, to plot the results of the tool

def plot_donut(df, ax, scenario, stakeholder, lim_in, lim_out, error, name):
    """
    The function generates a bar chart with the data supplied to it.

    :param panda.dataFrame df: The pa.output database, from which to fetch the plotted values.
    :param matplotlib.axe ax: the axis on which the graph is to be plotted.
    :param str scenario: The name of the scenario to be plotted.
    :param str stakeholder: The name of the stakeholder to be plotted.
    :param int lim_in: Minimum value at the center of the donut.
    :param int lim_out: Maximum value at the periphery of the donut.
    :param tuple error: a tuple containing the boolean status of whether to
        hide or show errorbar in (tuple[0]) and errorbar out (tuple[1]).
    :param str name: The name (of the scenario) to be displayed above the graph.
    """

    # Layout parameters
    ax.xaxis.grid(False)
    ax.yaxis.grid(False)
    ax.spines["start"].set_color("none")
    ax.grid(False)
    ax.axis('off')
    ax.set_title(name, fontsize=20, ha="center", va="center")
    COLOR_OUT_DONUT = "#08A04B"
    COLOR_BAR_M = "#CD7F32"
    COLOR_BAR_L = "#C04000"
    COLOR_DONUT = "#3A5F0B"

    # Recover labels IN and OUT for the donut
    df = df.loc[df['Stakeholders'] == stakeholder]
    value_scenario_out = df.loc[df['Category_ID'] == 'OUT', scenario].values
    value_scenario_in = df.loc[df['Category_ID'] == 'IN', scenario].values

    label_out_ = df.loc[df['Category_ID'] == 'OUT', 'Name'].values
    label_in_ = df.loc[df['Category_ID'] == 'IN', 'Name'].values

    error_out = df.loc[df['Category_ID'] == 'OUT', 'Errorbar'].values
    error_in = df.loc[df['Category_ID'] == 'IN', 'Errorbar'].values

    # Security if there is no values to plot.
    if len(label_out_) == 0 or len(label_in_) == 0:
        return
    # Definition of the geometrical parameters, in MAJUSCULE.
    DONUT = 2
    ANGLES_OUT = np.linspace(0, 2 * np.pi, len(label_out_), endpoint=False)
    ANGLES_IN = np.linspace(0, 2 * np.pi, len(label_in_), endpoint=False)
    WIDTH_OUT = 2 * np.pi / len(label_out_)  # Determine the width of each bar.
    OFFSET_OUT = np.pi / 2  # Determines where to place the first bar.
    WIDTH_IN = 2 * np.pi / len(label_in_)  # Determine the width of each bar.

    ##############################################################################################
    # a/ Labels
    # Add Labels (OUT) and text (IN)
    # Note the 'wrap()' function. The '10' means we want at most 10 consecutive letters in a word,
    # but the 'break_long_words' means we don't want to break words longer than 10 characters.
    for v in range(0, len(label_out_)): label_out_[v] = str(label_out_[v])
    for v in range(0, len(label_in_)): label_in_[v] = str(label_in_[v])
    label_out = ["\n".join(wrap(r, 10, break_long_words=False)) for r in label_out_]
    label_in = ["\n".join(wrap(r, 10, break_long_words=False)) for r in label_in_]

    # Add space for labels:
    XTICKS = ax.xaxis.get_major_ticks()
    for tick in XTICKS: tick.set_pad(6)
    # Add labels OUT
    ax.set_xticks(ANGLES_OUT)
    ax.set_xticklabels(label_out, size=6)

    # Add text for IN
    for i in range(0, len(label_in)):
        texte = label_in[i]
        angle = ANGLES_IN[i]
        ax.text(angle, -(DONUT / 2) - 2, texte, fontsize=6, ha="center", va="center")
    ############################################################################################
    # b/ Style and limits
    ax.set_ylim(-lim_in, lim_out)
    ax.set_theta_offset(OFFSET_OUT)  # Specify offset

    # Mask to differentiate values for outer circle
    mask_OUT_low = value_scenario_out < 0
    mask_OUT_in = (value_scenario_out >= 0) & (value_scenario_out < DONUT / 2)
    mask_OUT_out = (value_scenario_out >= DONUT / 2) & (value_scenario_out < lim_out)
    mask_OUT_high = value_scenario_out >= lim_out

    # Mask to differentiate values for inner circle
    mask_IN_low = value_scenario_in < 0
    mask_IN_in = (value_scenario_in >= 0) & (value_scenario_in < DONUT / 2)
    mask_IN_out = (value_scenario_in >= DONUT / 2) & (value_scenario_in < lim_in)
    mask_IN_high = value_scenario_in >= lim_in

    ##########################################################################################
    # d/ Plotting

    # Donut plotting
    ax.bar(0, DONUT / 2, color=COLOR_DONUT, alpha=1, width=2 * np.pi, zorder=0)
    ax.bar(0, -DONUT / 2, color=COLOR_DONUT, alpha=1, width=2 * np.pi, zorder=0)

    # Outer circle
    # Plotting of values outside the donut

    ax.bar(ANGLES_OUT[mask_OUT_high], value_scenario_out[mask_OUT_high],
           color=COLOR_BAR_L, alpha=0.6, width=WIDTH_OUT, zorder=-1)
    ax.bar(ANGLES_OUT[mask_OUT_out], value_scenario_out[mask_OUT_out],
           color=COLOR_BAR_M, alpha=0.6, width=WIDTH_OUT, zorder=-1)

    # Plotting of values inside the donut
    ax.bar(ANGLES_OUT[mask_OUT_in], -value_scenario_out[mask_OUT_in],
           color=COLOR_OUT_DONUT, alpha=1, width=WIDTH_OUT, zorder=1, bottom=DONUT / 2)
    ax.bar(ANGLES_OUT[mask_OUT_low], -DONUT / 2,
           color=COLOR_OUT_DONUT, alpha=1, width=WIDTH_OUT, zorder=1, bottom=DONUT / 2)

    # Plotting error bar
    if error[1]:
        ax.errorbar(ANGLES_OUT[mask_OUT_out], value_scenario_out[mask_OUT_out], yerr=error_out[mask_OUT_out],
                    capsize=1, capthick=4, color="red", linestyle='')
        ax.errorbar(ANGLES_OUT[mask_OUT_in], -value_scenario_out[mask_OUT_in], yerr=error_out[mask_OUT_in],
                    capsize=1, capthick=4, color="green", linestyle='')

    # Inner circle
    # Plotting of values inside the donut
    ax.bar(ANGLES_IN[mask_IN_high], -lim_in,
           color=COLOR_BAR_L, alpha=0.6, width=WIDTH_IN, zorder=-1)
    ax.bar(ANGLES_IN[mask_IN_out], -value_scenario_in[mask_IN_out],
           color=COLOR_BAR_M, alpha=0.6, width=WIDTH_IN, zorder=-1)

    # Plotting of values outside the donut
    ax.bar(ANGLES_IN[mask_IN_in], value_scenario_in[mask_IN_in],
           color=COLOR_OUT_DONUT, alpha=1, width=WIDTH_IN, zorder=1, bottom=-DONUT / 2)
    ax.bar(ANGLES_IN[mask_IN_low], DONUT / 2,
           color=COLOR_OUT_DONUT, alpha=1, width=WIDTH_IN, zorder=1, bottom=-DONUT / 2)

    # Plotting error bar
    if error[0]:
        ax.errorbar(ANGLES_IN[mask_IN_out], -value_scenario_in[mask_IN_out], yerr=error_in[mask_IN_out],
                    capsize=4, capthick=4, color="red", linestyle='')
        ax.errorbar(ANGLES_IN[mask_IN_in], value_scenario_in[mask_IN_in], yerr=error_in[mask_IN_in],
                    capsize=4, capthick=4, color="green", linestyle='')


######################################################################################################################
def plot_type_1(database_output, stakeholder, scenario_name):
    """ The plot_type_1 function creates the interactive page that will host the Donut graph for the scenario and the
    of the scenario and the impacted part.

    :param panda.dataFrame database_output: The pa.output database, from which to fetch the plotted values.

    :param str stakeholder: The name of the stakeholder to be displayed.

    :param str scenario_name: The name of the scenario being studied (its value is stored in self.stakeholder of a
        class *Parameters*)
    """

    col = "#add8e6"
    ############################################################
    '''1.a/ Figure creation'''
    fig, (legend, ax) = plt.subplots(1, 2, figsize=(15, 10), subplot_kw={"projection": "polar"}, label='Donut Tool')

    ############################################################
    '''1.b/ Buttons and interactivity'''
    # defining button and add its functionality

    ax_button1 = plt.axes([0.35, 0.05, 0.10, 0.06], label='Refresh')
    button_refresh = Button(ax_button1, 'Refresh', color=col)
    ax_button2 = plt.axes([0.1, 0.05, 0.10, 0.06], label='Go Back')
    button_goback = Button(ax_button2, 'Go Back', color=col)
    ax_button_errorbar = plt.axes([0.2, 0.05, 0.15, 0.06], label='ErrorBar ?')
    button_errorbar = CheckButtons(ax_button_errorbar, ('Errorbars IN', 'Errorbars OUT'), (False, True))

    # Building of max and min value limit sliders
    ax_slide_limit_out = plt.axes([0.2, 0.15, 0.20, 0.03], label='Parameters')
    s_factor_out = Slider(ax_slide_limit_out, 'limit OUT', valmin=1, valmax=20, valinit=10, valstep=1, color=col)
    ax_slide_limit_in = plt.axes([0.2, 0.2, 0.20, 0.03])
    s_factor_in = Slider(ax_slide_limit_in, 'limit IN', valmin=1, valmax=20, valinit=10, valstep=1, color=col)
    lim_out = s_factor_out.val
    lim_in = s_factor_in.val
    ax.set_ylim(-lim_in, lim_out)

    ############################################################
    '''1.c/ Update functions'''
    def back(val):
        plt.close()

    def update_1(val):
        limout = s_factor_out.val
        limin = s_factor_in.val
        ax.set_ylim(-limin, limout)
        fig.canvas.draw()

    def refresh_1(val):
        error = button_errorbar.get_status()
        ax.clear()
        limout = s_factor_out.val
        limin = s_factor_in.val
        ax.set_ylim(-limin, limout)

        df_legend = database_output.loc[database_output['Stakeholders'] == stakeholder]
        df_legend = df_legend[["Name", scenario_name]]
        if len(df_legend) > 0:
            legend.table(cellText=df_legend.values, colLabels=df_legend.columns, loc='center', label='Table of values')
        legend.grid(False)
        legend.axis('off')
        legend.set_title('Donut du projet (% de l\'objectif)',
                         loc='center', fontsize=20, ha="center", va="center", font="Bell MT")

        plot_donut(database_output, ax, scenario_name, stakeholder, limin, limout, error, scenario_name + ':' + stakeholder)
        fig.canvas.draw()

    def errorbar_check(val):
        error = button_errorbar.get_status()
        ax.clear()
        limout = s_factor_out.val
        limin = s_factor_in.val
        ax.set_ylim(-limin, limout)

        df_legend = database_output.loc[database_output['Stakeholders'] == stakeholder]
        df_legend = df_legend[["Name", scenario_name]]
        legend.table(cellText=df_legend.values, colLabels=df_legend.columns, loc='center', label='Table of values')
        legend.grid(False)
        legend.axis('off')
        legend.set_title('Donut du projet (% de l\'objectif)',
                         loc='center', fontsize=20, ha="center", va="center", font="Bell MT")

        plot_donut(database_output, ax, scenario_name, stakeholder, limin, limout, error, scenario_name + ':' + stakeholder)
        fig.canvas.draw()

    ############################################################
    '''1.d/ Drawing'''
    # Drawing Donut
    refresh_1(0)
    # Modification with interactivity
    s_factor_out.on_changed(update_1)
    s_factor_in.on_changed(update_1)
    button_refresh.on_clicked(refresh_1)
    button_errorbar.on_clicked(errorbar_check)
    button_goback.on_clicked(back)
    plt.show()

######################################################################################################################
def plot_type_2(database_output, scenario_name):
    """
    The plot_type_2() function creates the interactive page that will host the Donut graphs, for all 6 stakeholders,
    of the impacts on Endpoint capabilities on the scenario entered as a parameter.

    :param database_output: The pa.output database, from which to fetch the plotted values.

    :param str scenario_name: The name of the scenario studied
        (its value is stored in self.stakeholder of an object of class *Parameters*)
    """

    plt.rcParams.update({"font.family": "Bell MT"})
    plt.rc("axes", unicode_minus=False)  # Minus glyph is not available in Bell MT, This disables it, and uses a hyphen
    plt.rcParams["text.color"] = "#333333"
    col = "#add8e6"
    stakeholders = ['Local_Community', 'Society', 'Consumers', 'Workers', 'Chain_Value_Actors', 'Childrens']
    ############################################################
    '''2.a/ Figure creation'''
    fig, axes = plt.subplots(2, 3, figsize=(25, 10), subplot_kw={"projection": "polar"}, label='Donut Tool')
    fig.tight_layout(pad=10.0)

    ############################################################
    '''2.b/ Buttons and interactivity'''
    ax_button = plt.axes([0.85, 0.05, 0.10, 0.03], label='Go Back')
    button_goback = Button(ax_button, 'Go Back', color=col)
    ax_button1 = plt.axes([0.75, 0.05, 0.10, 0.03], label='Refresh')
    button_refresh = Button(ax_button1, 'Refresh', color=col)

    # Building of max and min value limit sliders
    ax_slide_limit_out = plt.axes([0.2, 0.05, 0.20, 0.03], label='Parameters')
    s_factor_out = Slider(ax_slide_limit_out, 'limit OUT', valmin=1, valmax=20, valinit=10, valstep=1, color=col)
    ax_slide_limit_in = plt.axes([0.5, 0.05, 0.20, 0.03])
    s_factor_in = Slider(ax_slide_limit_in, 'limit IN', valmin=1, valmax=20, valinit=10, valstep=1, color=col)
    limout = s_factor_out.val
    limin = s_factor_in.val
    ############################################################
    '''2.c/ Update functions'''
    def back(val):
        plt.close(fig)

    def update_2(val):
        limout = s_factor_out.val
        limin = s_factor_in.val
        for i in range(0, 2):
            for j in range(0, 3):
                ax = axes[i, j]
                ax.set_ylim(-limin, limout)
        fig.canvas.draw()

    def refresh_2(val):
        limout = s_factor_out.val
        limin = s_factor_in.val

        ############################################################
        '''2.d/ Drawing'''
        for i in range(0, 2):
            for j in range(0, 3):
                ax = axes[i, j]
                ax.clear()
                ax.set_ylim(-limin, limout)
                if 3*i+j < len(stakeholders):
                    plot_donut(database_output, ax, scenario_name, stakeholders[3*i+j], limin, limout, [False, False], stakeholders[3*i+j])
                else:
                    ax.grid(False)
                    ax.axis('off')
        fig.canvas.draw()


    '''1.d/ Drawing'''
    # Drawing Donut
    refresh_2(0)
    # Modification with interactivity
    s_factor_out.on_changed(update_2)
    s_factor_in.on_changed(update_2)
    button_refresh.on_clicked(refresh_2)
    button_goback.on_clicked(back)
    plt.show()
