"""
This python file contains the class CreatePathway and the methods necessary for this class:

- get_values() - fetch Value
- get_infos() - fetch Stakeholder, realisation factor, type, entries ID, entries names
- get_precision() - fetch dimensions of precision

- estimate_homogeneisation() - type 2 function, transform into mean notation
- estimate_statistical() - type 2 function, statistical model for normalisation
- calcul() - type 1 function, pathway evaluation.

Pathways entries and output are defined in  *pathway_data* database (see the Guide to study its structure).
We use the 'get_entry_list' method to obtain the values of the entry parameters.
We use the 'get_precision' method to obtain the precision of the value of each entry.

We use pathways calcul to return the output value of the pathway, depending on its ID.

1/ **Creating a new pathway ? (*more complex explanation in Contributor Guide*)**

A. To create a new pathway with ID_new as ID, start by adding its metadata in your pathway_data database.
There, make sure the entries are existing in TEMPLATE sheet. If not, add them.

B. Then, add an 'if self.ID == ID_new:' and the calculs of the new pathway.
The entries are automatically stored in a dictionary called e. To use an entry called 'entry' in the database, just type e['entry']

C. Finally, store the result in self.value

**2/ CreatePathway Class & functions**
"""

from math import exp, log

#######################################################################################################################

def get_values(LCI_table, id_variable):
    """ Fetch data of the variable with ID id_variable from the DataFrame dataframe_variables.

    It accepts three formats:
        - string (ex: Hello)
        - list (ex: [1,2,3])
        - array (ex: [[1,2],[A,B]])

    pathway_get_value return a string, a list of string, or an array of string. But in string type!

    :param panda.DataFrame LCI_table: a dataframe with the Life cycle inventory of either a scenario or a reference.
    :param int id_variable: The ID of the variable we want the data.
    :return value: The *string* or *list* or *array* containing the data of the variable.
    """

    # Take the value from the database
    value = str(LCI_table.loc[LCI_table['ID'] == id_variable, 'Value'].iloc[0])
    # Raise error if no value
    if value == 'nan':
        raise Exception('La variable Z d\'identifiant ' + id_variable + ' n\'a pas de valeur.')

    # Prevent error with coma
    value = value.replace(',', '.')

    # Transform to python list or array based on structure '[]' or '[[],[]]
    if value[0] == '[' and value[1] != '[':
        value = value.replace(' ', '').replace('[', '').replace(']', '').split(';')
    if value[0] == '[' and value[1] == '[':
        value = value[:-2]
        value = value.split(']')
        array = []
        for v in value:
            v = v[2:]
            v = v.split(';')
            array.append(v)
        value = array
    return value
#######################################################################################################################
class CreatePathway:
    """
    The attribute of CreatePathway class object are:

    - self.ID:
        The only parameter of the CreatePathway class.
        Its the Id of the pathway in the pathway_data database.
    - self.type:
        "1" if pathway evaluation use calcul(). "2, statistical_model" if it use estimate methods. Fetched using get_info()
    - self.stakeholder:
        Where the output stakeholder will be stored.
    - self.statistique_model :
        Fetched using get_info().
    - self.realisation :
        The pathway realisation factor. Forced on 1 for alpha version.
    - self.entry :
        List of entries ID, fetched using get_info().
    - self.name :
        List of entries name, fetched using get_info().

    - self.precision:
        A list of integer containing the precision fetched using get_precision().
    - self.value:
        Raw value of the evaluation of the pathway.
    - self.value_n:
        Final value after going through homogeneisation, normalisation, ponderation.

    :param int id_:
        The identifiant of the pathway we are creating.
    """

    def __init__(self, id_):
        self.ID = id_

        self.type = 0
        self.stakeholder = None
        self.statistique_model = 'linear'
        self.realisation = 1
        self.entry = {}
        self.name = []

        self.precision = None
        self.value = None
        self.value_n = None

######################################################################################################################
    def get_info(self, pathways_data):
        """
        get_info is a method of the class CreatePathway.
        It is used to fetch the data of the current pathway from *pathways_data* database.
        It will fetch: The stakeholder, the realisation factor, the pathways's type, the entries list of ID and names.

        :param pathways_data: a panda.DataFrame of the database of the information of every pathway. See *Pathways
            database* for further details.
        """
        # Get output stakeholder
        self.stakeholder = str(pathways_data.loc[pathways_data['Pathway_ID'] == self.ID, 'Output_stakeholder'].iloc[0])

        # Get realisation
        self.realisation = float(pathways_data.loc[pathways_data['Pathway_ID'] == self.ID, 'Realisation'].iloc[0])

        # Get output type (estimate or calcul evaluation ?)
        type_ = str(pathways_data.loc[pathways_data['Pathway_ID'] == self.ID, 'Type'].iloc[0])
        type_ = type_.replace(' ', '').replace('[', '').replace(']', '').split(';')
        self.type = int(type_[0])
        if type == 2:
            self.statistique_model = str(type_[1])

        # Get input list of entries ID
        id_ = str(pathways_data.loc[pathways_data['Pathway_ID'] == self.ID, 'Entry_ID'].iloc[0])
        id_ = id_.replace(' ', '').replace('[', '').replace(']', '').split(';')

        # Get list of entries name
        name_ = str(pathways_data.loc[pathways_data['Pathway_ID'] == self.ID, 'Entry_Name'].iloc[0])
        name_ = name_.replace(' ', '').replace('[', '').replace(']', '').split(';')
        self.entry = id_
        self.name = name_

#######################################################################################################################
# Function to get the precision value/s from the database. it expects a number or a list type.
    def get_precision(self, LCI_table):
        """
        get_precision  is a self function of the class CreatePathway.
        It is used to fetch the precision's data of the pathway's inputs (which IDs are stored in self.entry).
        Then it will calcul the mean of each precision category.

        :param LCI_table: a panda.DataFrame of the Life cycle inventory of either the scenario or the reference.
        """
        x = [0, 0, 0, 0]
        self.precision = x
        for id_ in self.entry:
            x[0] += int(LCI_table.loc[LCI_table['ID'] == id_, 'Reliability of the source (/5)'].iloc[0])
            x[1] += int(LCI_table.loc[LCI_table['ID'] == id_, 'Temporal precision (/5)'].iloc[0])
            x[2] += int(LCI_table.loc[LCI_table['ID'] == id_, 'Spatial precision (/5)'].iloc[0])
            x[3] += int(LCI_table.loc[LCI_table['ID'] == id_, 'Technical performance (/5)'].iloc[0])

        for y in range(0, len(x)):
            self.precision[y] = x[y] / len(self.entry)

#######################################################################################################################
# Function to calculate the output of a pathways type 2,

    def estimate_homogeneisation(self, LCI_table):
        """The function estimate_homogeneisation is the 1st/2 function of the estimate evaluation method.
        It aims at importing the entries values, and translating the notation into a mean notation.

        :param LCI_table: a panda.DataFrame of the Life cycle inventory of either the scenario or the reference.
        """
        summ = 0
        # Aggregation for pathway
        for i in range(0, len(self.entry)):
            e = str(get_values(LCI_table, self.entry[i]))
            e = e.replace('A', '').replace('D', '-').replace('N', '0')
            e = int(e)
            self.entry[i] = e
            summ += e
        self.value = summ / len(self.entry)

#######################################################################################################################
    def estimate_statistical(self, ref):
        """The function estimate_statistical is the 2nd/2 function of the estimate evaluation method.
        Its goal is to transform the value using the predetermined statistical model.

        :param ref: A Class CreatePathway object that belong to the chosen reference, with the same name & ID as
            the pathway that called the pathway_estimate function.
        """
        Delta = ref.value * ref.realisation - self.value * self.realisation
        if self.statistique_model == 'linear':
            self.value_n = 1 + Delta / 6

        elif self.statistique_model == 'square':
            a, b, c = 1/36, 1/3, 1
            self.value_n = a * Delta**2 + b * Delta + c

        elif self.statistique_model == 'exponential':
            self.value_n = exp(Delta/3)

        elif self.statistique_model == 'logarithmic':
            self.value_n = 1 + log(1 + Delta/18)

        else:
            # Using linear model with gain dissymetry theory

            if self.value >= ref.value:
                self.value_n = 1 + Delta*2 / 6
            if self.value < ref.value:
                self.value_n = (1 + Delta / 6)

#######################################################################################################################
# Function to calculate the output of a pathways type 1, depending on self.ID value.

    def calcul(self, LCI_table):
        """ The calcul() method translates the calculating method to evaluate a pathway.

        The function stores input parameters in a dictionary named 'e'.
        So, to use the variable 'degrad_eaux' in the code, simply use e['degrad_eaux'].

        In the function, we look at the pathway identifier and perform the corresponding calculation module.
        Finally, we store the result in self.value.

         .. WARNING::
            Be careful to name inputs in the same way in the LCI and in the calcul() function.

        :param LCI_table: a panda.DataFrame of the Life cycle inventory of either the scenario or the reference.

        """
        # Value attribution - généralisé
        e = {}
        for i in range(0, len(self.entry)):
            e[self.name[i]] = get_values(LCI_table, self.entry[i])
            if type(e[self.name[i]]) == str:
                e[self.name[i]] = float(e[self.name[i]])

        # Name: Global warming impact / ID:1
        if self.ID == 1:
            self.value = e['ges_emissions_build'] + e['ges_emissions_uses']
            self.value += - e['duration'] * float(e['carbon_storage'][1])

        # Name: Water stress  /  ID:2
        if self.ID == 2:
            water_pressure = e['duration'] * (e['water_use'] - e['water_produce']) * 12
            self.value = e['water_stress_factor'] + water_pressure/(e['water_storage_in_soil']*50*12)

        # Name: Land Change /  ID:3
        if self.ID == 3:
            irreversible = (e['arable_urbanisation']-e['renaturalisation'])
            forest = e['deforestation']*e['land_stress_factor']
            self.value = (irreversible+forest)/100

        # Name: Water stress  /  ID:5
        if self.ID == 5:
            water_pressure = e['duration'] * (e['water_use'] - e['water_produce']) * 12
            self.value = e['water_stress_factor'] + water_pressure/(e['water_storage_in_soil']*50*12)

        # Name: Land Change /  ID:6
        if self.ID == 6:
            irreversible = (e['arable_urbanisation']-e['renaturalisation'])
            forest = e['deforestation']*e['land_stress_factor']
            self.value = (irreversible+forest)/100
