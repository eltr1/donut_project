"""
This code contains the structure of the tool.
It calls the different functions of the other python codes to create the wanted results !
It contains the definition of the application using tkinter.

 .. WARNING::
    1/ Make sure all dependencies are installed. Use the following code in a command line:
    py -m pip install -r requirements.txt

**1/ Here is a quick explanation of the variables used:**

pathways_data  -  a pd.Dataframe of the inputs, outputs and parameters of each "active" pathways
database_endpoint_impact - a pd.Dataframe with the parameters used for each environnemental and social impact.

LCI -  Life cycle inventory of the selected scenario.
LCI_ref - Life cycle inventory of the selected reference.
pathways_list - list of the CreatePathway objects of the selected scenario, each object represent a pathway.
pathways_list_ref - list of the CreatePathway objects of the selected reference, each object represent a pathway.

database_output - a pd.Dataframe, similar to 'database_endpoint_impacts' but with new columns for the impact values of
the reference, and each scenario.


**2/ Structure**

PART 1: Function for the application
generate()
action_stakeholder_list()
action_scenario_list()
action_reference_list()
stakeholder_command()
import_file()

PART 2: Write the application
General Home layout
Application Parameters
Home buttons

**3/ Functions**
"""

import os
import pandas as pd
import tkinter as tk
from tkinter import ttk
from tkinter.filedialog import askopenfilename
from Code.Graph import *
from Code.Assessment import *
import datetime

#######################################################################################################################

################################################
# Part 1: Function for the application
def generate():
    """ Generate will launch a new window with a matplotlib graph containing the donut with the
    specified parameters (stored in pa Class object).

    *generate()* will get the file name for VarString file_path defined in the *Import* tk.Button
    Then it will load the pathway_data, database_endpoint_impact, reference and scenario databases.

    .. WARNING::

        Make sure the name of the pathways_data Database is ***4.Pathways_recap*** and the
        database for endpoints impacts is named ***5.Graph_recap***

    Then generate() will launch the calcul module from pathways.py and weighing.py
    Finally generate() will launch the plotting function from Graph.py, according to pa.mode (specific or multiple view)
    """

    # Loading comment
    loading_status.set('[5%] loading the Databases')
    gen.update_idletasks()

    # PART 1: Load Data ---------------
    # Excel parameters :
    name_file = file_path.get()
    # Get the pathways parameters and definition:
    pathways_data = pd.read_excel(name_file, 'pathways_data')
    # Get the Endpoint impact characterisation and definition
    database_endpoint_impact = pd.read_excel(name_file, 'database_endpoint_impact')

    # Loading comment
    loading_status.set('[25%] loading the Scenarios')
    gen.update_idletasks()

    # PART 2: Load Scenario's data -----------------
    LCI = pd.read_excel(name_file, pa.scenario_name)
    LCI_ref = pd.read_excel(name_file, pa.ref_name)


    # Loading comment
    loading_status.set('[50%] Mega-calcul mode activated')
    gen.update_idletasks()

    # PART 3: Calculs -----------------
    pathways_list = get_pathway_list(pathways_data, LCI)
    pathways_list_ref = get_pathway_list(pathways_data, LCI_ref)
    database_output = get_comparison(database_endpoint_impact, pathways_list, pa.scenario_name, pathways_list_ref,
                                     pa.error_shown)
    pa.output = database_output
    # Loading comment
    loading_status.set('[95%] Plotting the Donut...')
    gen.update_idletasks()

    # PART 4: Plotting ----------------
    if pa.mode == 2:
        plot_type_2(database_output, pa.scenario_name)
    if pa.mode == 1:
        plot_type_1(database_output, pa.stakeholder, pa.scenario_name)

    # Loading comment
    loading_status.set('[100%] Donut ready !')


def action_stakeholder_list(event):
    """ The action_stakeholder_list function will update the stakeholder attribute in pa class object (pa.stakeholder).
    """
    pa.stakeholder = stakeholder_var.get()


def action_scenario_list(event):
    """ The action_scenario_list function will update the scenario attribute in pa class object (pa.scenario_name).
    """
    pa.scenario_name = scenario_var.get()


def action_reference_list(event):
    """ The action_reference_list function will update the reference attribute in pa class object (pa.ref_name).
    """
    pa.ref_name = reference_var.get()



def stakeholder_command():
    """ This function will hide or show the stakeholder list button depending on whether specific
    stakeholder is checked.
    It will also store in pa.mode which mode is being chosen (multiple or specific).
    """
    pa.mode = stakeholders_type.get()
    if pa.mode == 1:
        stakeholder_list.pack()
    else:
        stakeholder_list.pack_forget()

def export():
    """ It export a panda.DataFrame to an excel file.
    It export in the same folder as main.py.
    The exported name is "Export_year_month_day_hour_minute_second"

    .. WARNING::
        You should Generate a result with *Generate* before trying to export the results.

    """
    d = datetime.datetime.now()
    da = str(d.year) + "_" + str(d.month) + "_" + str(d.day) + "_" + str(d.hour)
    da += "_" + str(d.minute) + "_" + str(d.second)

    if pa.output is not None:
        pa.output.to_excel('Export_' + da + '.xlsx', 'Export_data')
        loading_status.set('Succefully exported')
        gen.update_idletasks()
    if pa.output is None:
        loading_status.set('Please, use Generate first.')
        gen.update_idletasks()


def import_file():
    """ This function is used to select the file which contains the project data you want to evaluate
    Currents type of files supported:
    - EXCEL (.xlsx)
    - Open Document Format (.odf)

    .. WARNING::

        1/ Make sure the file you load is in the same directory as main.py

        2/ Make sure all your scenario sheet's name start with ***SCENARIO***
        So the sheet name should be SCENARIO-your_scenario_name

    This import() function will also update the reference and scenario list of the user's interface.
    """

    # Type of files supported
    filetypes = (
        ('All files', '*.*'),
        ('Excel files', '*.xlsx'),
        ('Open Document Spreadsheet', '*.ods')
    )

    # Function to load the filename. Make sure your file is in the same directory as main.py
    filepath = askopenfilename(
        title='Open files',
        initialdir=os.getcwd(),
        filetypes=filetypes)
    if len(filepath) > 30:
        filepath = '...' + filepath[-30:]
    file_path.set(os.path.basename(filepath))

    # Get the Data for each scenario:
    # Make sure all your sheets of your scenarios start with SCENARIO
    # (so the name of the sheet is SCENARIO-your_name)
    pa.List_name = []
    scenarios = pd.Series(data=pd.ExcelFile(file_path.get()).sheet_names)
    is_a_scenario = scenarios.str.startswith('SCENARIO')
    for i in range(0, len(scenarios)):
        if not is_a_scenario.values[i]:
            scenarios = scenarios.drop(i)
        else:
            pa.List_name.append(scenarios[i])

    scenario_list['values'] = pa.List_name
    reference_list['values'] = pa.List_name

def dim_status():
    """This function will store the precision dimension choice (made in Menu window) in pa.error_shown."""
    pa.error_shown = dim.get()
###################################################################################################################
# Part 2: Write the Application

# If loop to protect the script from autodoc documentation (leads to errors)
if __name__ == '__main__':

    ################################################
    # General Home layout
    root = tk.Tk()
    root.title('Donut Tool')
    root.geometry('400x600')
    root.configure(background='white')

    # Sub-frame for left-frame import file
    importlabel = tk.LabelFrame(root, text="Import files", padx=20, pady=20)
    importlabel.pack(fill="both", expand="yes")

    # Sub-frame for left-frame parameters categories
    parameters = tk.LabelFrame(root, text="Parameters Comparison", padx=20, pady=20)
    parameters.pack(fill="both", expand="yes")

    mode = tk.LabelFrame(root, text="Mode", padx=20, pady=20)
    mode.pack(fill="both", expand="yes")

    prec = tk.LabelFrame(root, text="Precision dimension", padx=20, pady=20)
    prec.pack(fill="both", expand="yes")

    gen = tk.LabelFrame(root, text="Let's draw !", padx=20, pady=20)
    gen.pack(fill="both", expand="yes")

    #################################################
    # Application Parameters

    # Initialize class Parameters in a class object called 'pa'
    pa = Parameters()
    stakeholders = ['Local_Community', 'Society', 'Consumers', 'Workers', 'Chain_Value_Actors', 'Childrens']



    ################################################
    # Home buttons

    ###################
    # Left-frame / Sub-frame Parameters Comparison
    reference_var = tk.StringVar()
    scenario_var = tk.StringVar()

    # List for scenarios
    tk.Label(parameters, text='Quel scenario ?').grid(column=0, row=0)
    scenario_list = ttk.Combobox(parameters, values=pa.List_name, textvariable=scenario_var)
    scenario_list.current(0)
    scenario_list.grid(column=0, row=1)
    scenario_list.bind("<<ComboboxSelected>>", action_scenario_list)

    # List for reference
    tk.Label(parameters, text='Quel reference ?').grid(column=1, row=0, padx=50)
    reference_list = ttk.Combobox(parameters, values=pa.List_name, textvariable=reference_var)
    reference_list.current(0)
    reference_list.grid(column=1, row=1, padx=50)
    reference_list.bind("<<ComboboxSelected>>", action_reference_list)

    ###################
    # Left-frame / Sub-frame Mode
    stakeholders_type = tk.IntVar()
    stakeholder_var = tk.StringVar()

    # Stakeholders buttons
    multiple_stakeholder = tk.Radiobutton(mode, text="Multiple stakeholder", variable=stakeholders_type,
                                          value=2, command=stakeholder_command)
    multiple_stakeholder.pack()
    specific_stakeholder = tk.Radiobutton(mode, text="Specific stakeholder", variable=stakeholders_type,
                                          value=1, command=stakeholder_command)
    specific_stakeholder.pack()

    # Dimension precision
    dim = tk.IntVar()
    dimension1 = tk.Radiobutton(prec, text="Reliability of the source", variable=dim,
                                          value=0, command=dim_status).grid(column=0, row=0)
    dimension2 = tk.Radiobutton(prec, text="Temporal precision", variable=dim,
                               value=1, command=dim_status).grid(column=1, row=0, padx=50)
    dimension3 = tk.Radiobutton(prec, text="Geographical precision", variable=dim,
                               value=3, command=dim_status).grid(column=0, row=1)
    dimension4 = tk.Radiobutton(prec, text="Technical resilience", variable=dim,
                               value=4, command=dim_status).grid(column=1, row=1, padx=50)

    # Stakeholders list
    stakeholder_list = ttk.Combobox(mode, values=stakeholders, textvariable=stakeholder_var)
    stakeholder_list.current(0)
    stakeholder_list.bind("<<ComboboxSelected>>", action_stakeholder_list)
    stakeholder_list.pack()

    ###################
    # Left-frame / Sub-frame Let's draw !
    loading_status = tk.StringVar()
    loading_status.set('')
    tk.Button(gen, text='Generate', command=generate).pack(side=tk.LEFT)
    tk.Label(gen, textvariable=loading_status).pack(side=tk.RIGHT)

    loading_status = tk.StringVar()
    loading_status.set('')
    tk.Button(gen, text='Export', command=export).pack(side=tk.LEFT)
    tk.Label(gen, textvariable=loading_status).pack(side=tk.RIGHT)

    ###################
    # Left-frame / Sub-frame Import files
    file_path = tk.StringVar()
    file_path.set("")
    tk.Label(importlabel, text='Please use a template table, see Wiki for details...').pack()
    tk.Button(importlabel, text='Import', command=import_file).pack(side=tk.LEFT)
    tk.Label(importlabel, textvariable=file_path).pack(side=tk.RIGHT)

    root.mainloop()
